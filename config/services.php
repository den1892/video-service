<?php
/**
 * Created by PhpStorm.
 * User: basilliyc
 * Date: 05.06.19
 * Time: 14:17
 */

use App\DependencyInjection\CompilerPass\AppCompilerPass;
use App\InputHandling\CustomObjectData\CustomObjectDataInterface;
use FFMpeg\FFMpeg;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use App\EventSubscriber\EntityEventSubscriber;
use App\Video\VideoFile\VideoFile;
use Symfony\Component\DependencyInjection\Reference;

return function(ContainerConfigurator $containerConfigurator, ContainerBuilder $containerBuilder)
{


    /**
     * Container compiler pass
     */
    $containerBuilder->addCompilerPass(new AppCompilerPass());

    $containerBuilder->autowire(FFMpeg::class)
        ->setFactory([FFMpeg::class, 'create'])
        ->setArgument('$configuration', [
            'ffmpeg.binaries'  => '%env(FFMPEG_BINARY_PATH)%',
            'ffprobe.binaries' => '%env(FFPROBE_BINARY_PATH)%'
        ])
        ;

    /**
     * Doctrine event subscriber
     */
    $containerBuilder
        ->autowire(EntityEventSubscriber::class)
        ->addTag('doctrine.event_subscriber');

    /**
     * VideoFileManager
     */
    $containerBuilder
        ->autowire(VideoFile::class)
        ->setArgument('$publicPath', 'video')
        ->setArgument('$fullPathToPublic', '%kernel.root_dir%/../public')
        ->setArgument('$FFMpeg', new Reference(FFMpeg::class))
        ;
    /**
     * CustomObjectDataInterface
     */
    $containerBuilder->registerForAutoconfiguration(CustomObjectDataInterface::class)
        ->addTag(CustomObjectDataInterface::TAG_NAME);

   // $containerBuilder->autowire(\App\InputHandling\CustomObjectData\VideoSaveObjectData::class)->setPublic(true);

};