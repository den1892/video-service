FROM ubuntu:18.04

ENV DEBIAN_FRONTEND=noninteractive
RUN ln -fs /usr/share/zoneinfo/Europe/Amsterdam /etc/localtime

RUN apt update && apt install -y tzdata php7.2 php7.2-fpm php7.2-curl php7.2-xml php7.2-mbstring php7.2-zip php7.2-mysql composer nginx supervisor

RUN mkdir -p /var/run/php
COPY ./configs/supervisord.conf /etc/supervisor/supervisord.conf
COPY ./configs/nginx.conf /etc/nginx/nginx.conf

WORKDIR /app

COPY composer.json composer.lock ./
RUN composer install --prefer-dist --no-interaction

COPY . .
RUN chown www-data -R var

CMD /usr/bin/supervisord -n -c /etc/supervisor/supervisord.conf