<?php


namespace App\Validator;


use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\ConstraintViolationListInterface;

class ValidationResult
{

    /**
     * @var ConstraintViolationListInterface
     */
    private $constraintViolationList;

    public function __construct(ConstraintViolationListInterface $constraintViolationList)
    {
        $this->constraintViolationList = $constraintViolationList;
    }

    /**
     * @return ConstraintViolationList
     */
    public function getConstraintViolationList(): ConstraintViolationListInterface
    {
        return $this->constraintViolationList;
    }

    /**
     * @return bool
     */
    public function isValid(): bool
    {
        return count($this->constraintViolationList) === 0;
    }

}