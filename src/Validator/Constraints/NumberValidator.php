<?php


namespace App\Validator\Constraints;


use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class NumberValidator extends ConstraintValidator
{

    /**
     * Checks if the passed value is valid.
     *
     * @param mixed $value The value that should be validated
     * @param Constraint $constraint The constraint for the validation
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof Number) {
            throw new UnexpectedTypeException($constraint, Number::class);
        }
        if (null === $value || '' === $value) {
            return;
        }

        if (!is_numeric($value)){
            $this->context->buildViolation($constraint->message)->addViolation();
        }
    }
}