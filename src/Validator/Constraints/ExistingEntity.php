<?php


namespace App\Validator\Constraints;


use Symfony\Component\Validator\Constraint;

class ExistingEntity extends Constraint
{

    /**
     * @var string
     */
    public $message = 'The entity is not found for this value.';

    /**
     * @var string
     */
    public $propertyName;

    /**
     * @var string
     */
    public $entityClass;

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param string $message
     * @return ExistingEntity
     */
    public function setMessage(string $message): self
    {
        $this->message = $message;
        return $this;
    }

    /**
     * @return string
     */
    public function getPropertyName(): string
    {
        return $this->propertyName;
    }

    /**
     * @param string $propertyName
     * @return ExistingEntity
     */
    public function setPropertyName(string $propertyName): self
    {
        $this->propertyName = $propertyName;
        return $this;
    }

    /**
     * @return string
     */
    public function getEntityClass(): string
    {
        return $this->entityClass;
    }

    /**
     * @param string $entityClass
     * @return ExistingEntity
     */
    public function setEntityClass(string $entityClass): self
    {
        $this->entityClass = $entityClass;
        return $this;
    }

    public function getRequiredOptions()
    {
        return ['propertyName', 'entityClass'];
    }

}