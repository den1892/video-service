<?php


namespace App\Validator\Constraints;


use Symfony\Component\Validator\Constraint;

class Number extends Constraint
{

    public $message = 'This value should be of type number.';

}