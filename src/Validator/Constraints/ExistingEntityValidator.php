<?php


namespace App\Validator\Constraints;


use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Exception\UnexpectedValueException;

class ExistingEntityValidator extends ConstraintValidator
{

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Checks if the passed value is valid.
     *
     * @param mixed $value The value that should be validated
     * @param Constraint $constraint The constraint for the validation
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof ExistingEntity) {
            throw new UnexpectedTypeException($constraint, ExistingEntity::class);
        }
        if (null === $value || '' === $value) {
            return;
        }
        if (!is_string($value) && !is_integer($value)) {
            throw new UnexpectedValueException($value, 'string|integer');
        }

        $repository = $this->entityManager->getRepository($constraint->getEntityClass());
        $data = $repository->findOneBy([$constraint->getPropertyName()=>$value]);
        if (null === $data){
            $this->context->buildViolation($constraint->getMessage())
                ->setParameters([
                    '{{entityName}}'
                ])
                ->addViolation();
        }

    }
}