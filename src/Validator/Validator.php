<?php


namespace App\Validator;


use Symfony\Component\Validator\Validator\ValidatorInterface;

class Validator
{

    /**
     * @var ValidatorInterface
     */
    private $validator;

    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    /**
     * @param $value
     * @param null $constraints
     * @param null $groups
     * @return ValidationResult
     */
    public function validate($value, $constraints = null, $groups = null): ValidationResult
    {
        return new ValidationResult($this->validator->validate($value, $constraints, $groups));
    }

}