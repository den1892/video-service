<?php


namespace App\User\TokenManager;


use App\Entity\User;

interface TokenManagerInterface
{

    public function setToken(User $user): void;

}