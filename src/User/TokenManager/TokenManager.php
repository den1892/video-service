<?php


namespace App\User\TokenManager;


use App\Entity\User;

class TokenManager implements TokenManagerInterface
{

    public function setToken(User $user): void
    {
        $token = rtrim(strtr(base64_encode(random_bytes(32)), '+/', '-_'), '=');
        $user->setToken($token);
    }
}