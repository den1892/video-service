<?php


namespace App\User;


use App\Entity\User;
use App\Repository\User\UserRepositoryInterface;
use App\User\PasswordManager\PasswordManager;
use App\User\TokenManager\TokenManagerInterface;

class UserManager
{

    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;
    /**
     * @var PasswordManager
     */
    private $passwordManager;
    /**
     * @var TokenManagerInterface
     */
    private $tokenManager;

    public function __construct(TokenManagerInterface $tokenManager,UserRepositoryInterface $userRepository, PasswordManager $passwordManager)
    {
        $this->userRepository = $userRepository;
        $this->passwordManager = $passwordManager;
        $this->tokenManager = $tokenManager;
    }

    /**
     * @param User $user
     * @param string $password
     */
    public function registerUser(User $user, string $password): void
    {
        $this->passwordManager->setPassword($user, $password);
        $this->userRepository->saveUser($user);
    }

    /**
     * @param User $user
     */
    public function saveUser(User $user): void
    {
        $this->userRepository->saveUser($user);
    }

    /**
     * @param User $user
     */
    public function generateToken(User $user): void
    {
        $this->tokenManager->setToken($user);
    }

    /**
     * @param string $email
     * @param string $password
     * @return User|null
     */
    public function findUserByAuth(string $email, string $password): ?User
    {
        $user = $this->userRepository->findByEmail($email);
        if (null !== $user && $this->passwordManager->validatePassword($user, $password)){
            return $user;
        }
        return null;
    }

}