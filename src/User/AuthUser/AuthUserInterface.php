<?php


namespace App\User\AuthUser;


use App\Entity\User;

interface AuthUserInterface
{

    public function currentUser(): ?User;

}