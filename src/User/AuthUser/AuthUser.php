<?php


namespace App\User\AuthUser;


use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class AuthUser implements AuthUserInterface
{

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * AuthUser constructor.
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @return User|null
     */
    public function currentUser(): ?User
    {
        if (null === $this->tokenStorage->getToken()){
            return null;
        }
        $user = $this->tokenStorage->getToken()->getUser();
        if (is_object($user) && $user instanceof User){
            return $user;
        }
        return null;
    }
}