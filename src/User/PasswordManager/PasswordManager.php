<?php


namespace App\User\PasswordManager;


use App\Entity\User;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class PasswordManager implements PasswordManagerInterface
{

    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * @param User $user
     * @param string $password
     * @return bool
     */
    public function validatePassword(User $user, string $password): bool
    {
        return $this->passwordEncoder->isPasswordValid($user, $password);
    }

    /**
     * @param User $user
     * @param string $password
     */
    public function setPassword(User $user, string $password): void
    {
        $passwordEncoded = $this->passwordEncoder->encodePassword($user, $password);
        $user->setPassword($passwordEncoded);
    }
}