<?php


namespace App\User\PasswordManager;


use App\Entity\User;

interface PasswordManagerInterface
{
    /**
     * @param User $user
     * @param string $password
     * @return bool
     */
    public function validatePassword(User $user, string $password): bool;

    /**
     * @param User $user
     * @param string $password
     */
    public function setPassword(User $user, string $password): void;

}