<?php
/**
 * Created by PhpStorm.
 * User: programmer_5
 * Date: 4/25/19
 * Time: 1:21 PM
 */

namespace App\Command;


use App\Entity\Area;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;


class TestCommand extends Command
{

    protected static $defaultName = 'app:test';
    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(ContainerInterface $container, string $name = null)
    {
        parent::__construct($name);
        $this->container = $container;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

    }

    private function getEntityManager(): EntityManager
    {
        return $this->container->get('doctrine')->getManager();
    }


}