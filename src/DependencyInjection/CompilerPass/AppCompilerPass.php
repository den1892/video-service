<?php


namespace App\DependencyInjection\CompilerPass;


use App\InputHandling\CustomObjectData\CustomObjectDataInterface;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class AppCompilerPass implements CompilerPassInterface
{

    /**
     * You can modify the container here before it is dumped to PHP code.
     */
    public function process(ContainerBuilder $container)
    {
        $customObjectDefinitions = $container->findTaggedServiceIds(CustomObjectDataInterface::TAG_NAME);
        foreach ($customObjectDefinitions as $id=>$tags){
            $definition = $container->findDefinition($id);
            $definition->setShared(false)->setPublic(true);
        }
    }
}