<?php


namespace App\Controller;


use App\InputHandling\CustomObjectData\CategoryFilterObjectData;
use App\InputHandling\Serializer\CategorySerializer;
use App\Repository\Category\CategoryRepositoryInterface;
use App\Repository\Category\CategoryFilterData;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/category")
 * Class CategoryController
 * @package App\Controller
 */
class CategoryController extends ApiBaseControllerAbstract
{

    /**
     * @Route("", methods={"GET"})
     * @param CategorySerializer $categorySerializer
     * @param CategoryFilterObjectData $filterObjectData
     * @param CategoryRepositoryInterface $categoryRepository
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function list(CategorySerializer $categorySerializer, CategoryFilterObjectData $filterObjectData, CategoryRepositoryInterface $categoryRepository)
    {
        $filterData = new CategoryFilterData();

        $this->denormalizeRequestQuery($filterObjectData);

        $validationResult = $this->validate($filterObjectData);
        if ($validationResult->isValid()){
            $filterObjectData->loadFilterData($filterData);
        } else {
            return $this->responseWithValidation([], $validationResult->getConstraintViolationList());
        }

        $categories = $categoryRepository->getList(null, $filterData);
        $normalizingData = $categorySerializer->normalize($categories);

        return $this->responseList($normalizingData);
    }

}