<?php


namespace App\Controller;


use App\Entity\User;
use App\InputHandling\CustomObjectData\UserAuthCustomObject;
use App\InputHandling\CustomObjectData\UserRegistrationCustomObject;
use App\User\UserManager;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends ApiBaseControllerAbstract
{

    /**
     * @Route("/registration", methods={"POST"})
     * @param UserRegistrationCustomObject $customObject
     * @param UserManager $userManager
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function registration(UserRegistrationCustomObject $customObject, UserManager $userManager)
    {
        $user = new User();

        $this->denormalizeRequest(UserRegistrationCustomObject::class, $customObject);
        $validationResult = $this->validate($customObject);

        if ($validationResult->isValid()){
            $customObject->loadUser($user);
            $userManager->registerUser($user, $customObject->getPassword());
        }

        return $this->responseWithValidation([], $validationResult->getConstraintViolationList());
    }

    /**
     * @Route("/auth", methods={"POST"})
     * @param UserAuthCustomObject $customObject
     * @param UserManager $userManager
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function auth(UserAuthCustomObject $customObject, UserManager $userManager)
    {
       $resultData = [];

       $this->denormalizeRequest(UserAuthCustomObject::class, $customObject);
       $validationResult = $this->validate($customObject);
       if ($validationResult->isValid()){
           $user = $customObject->findUser();
           $userManager->generateToken($user);
           $userManager->saveUser($user);
           $resultData['token'] = $user->getToken();
       }
       return $this->responseWithValidation($resultData, $validationResult->getConstraintViolationList());
    }

}