<?php
/**
 * Created by PhpStorm.
 * User: basilliyc
 * Date: 04.06.19
 * Time: 13:41
 */

namespace App\Controller;

use App\Api\ApiResponseService;
use App\Api\Models\ErrorsIterator;
use App\InputHandle\InputHandleFactory\InputHandleFactory;
use App\InputHandling\Serializer\DefaultSerializer;
use App\Pagination\Interfaces\PaginationSettingsInterface;
use App\Pagination\Pagination;
use App\Pagination\PaginationService;
use App\Pagination\PaginationSettings;
use App\Serializer\ContextBuilder;
use App\Serializer\SerializerBuilder;
use App\Validator\ValidationResult;
use App\Validator\Validator;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Doctrine\Common\Persistence\ObjectManager;

abstract class ApiBaseControllerAbstract extends AbstractController
{

    const DEFAULT_LIMIT_ITEMS = 10;

    const MAX_LIMIT_ITEMS = 20;

    /**
     * @var RequestStack
     */
    private $requestStack;
    /**
     * @var PaginationService
     */
    private $paginationService;
    /**
     * @var ApiResponseService
     */
    private $apiResponseService;
    /**
     * @var DefaultSerializer
     */
    private $defaultSerializer;
    /**
     * @var Validator
     */
    private $validator;

    public function __construct(Validator $validator, DefaultSerializer $defaultSerializer,RequestStack $requestStack, PaginationService $paginationService, ApiResponseService $apiResponseService)
    {
        $this->requestStack = $requestStack;
        $this->paginationService = $paginationService;
        $this->apiResponseService = $apiResponseService;
        $this->defaultSerializer = $defaultSerializer;
        $this->validator = $validator;
    }

    /**
     * @return PaginationSettings
     */
    protected function getPaginationSettings(): PaginationSettings
    {
        $paginationSettings = new PaginationSettings();

        $request = $this->requestStack->getCurrentRequest();
        $page = (int)$request->get('page');
        $limit = (int)$request->get('limit');
        if (self::MAX_LIMIT_ITEMS < $limit) {
            $limit = self::MAX_LIMIT_ITEMS;
        } else if (0 === $limit) {
            $limit = self::DEFAULT_LIMIT_ITEMS;
        }

        $paginationSettings->setLimitItems($limit);
        $paginationSettings->setCurrentPageNumber($page);
        return $paginationSettings;
    }

    /**
     * @param QueryBuilder $queryBuilder
     * @return Pagination
     */
    protected function paginationByQuery(QueryBuilder $queryBuilder)
    {
        $paginationSettings = $this->getPaginationSettings();
        $this->paginationService->handleQuery($queryBuilder, $paginationSettings);
        return $this->paginationService->getPagination($paginationSettings);
    }

    /**
     * @return SerializerBuilder
     */
    protected function serializerBuilder(): SerializerBuilder
    {
        return new SerializerBuilder();
    }

    /**
     * @return ContextBuilder
     */
    protected function contextBuilder(): ContextBuilder
    {
        return new ContextBuilder();
    }

    /**
     * @param $data
     * @param $className
     * @param null $existingObject
     * @return object
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    protected function denormalizeData($data, $className, $existingObject=null)
    {
       return $this->defaultSerializer->denormalize($data, $className, $existingObject);
    }

    /**
     * @param $className
     * @param null $existingObject
     * @return object
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    protected function denormalizeRequest($className, $existingObject=null)
    {
        return $this->defaultSerializer->denormalizeRequest($this->requestStack->getCurrentRequest(), $className, $existingObject);
    }

    /**
     * @param $className
     * @param null $existingObject
     * @return object
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    protected function denormalizeRequestFile($className, $existingObject=null)
    {
        return $this->defaultSerializer->denormalizeRequestFile($this->requestStack->getCurrentRequest(), $className, $existingObject);
    }
    /**
     * @param $className
     * @param null $existingObject
     * @return object
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    protected function denormalizeRequestQuery($className, $existingObject=null)
    {
        return $this->defaultSerializer->denormalizeRequestQuery($this->requestStack->getCurrentRequest(), $className, $existingObject);
    }


    /**
     * @param $value
     * @param null $constraints
     * @param null $groups
     * @return ValidationResult
     */
    protected function validate($value, $constraints = null, $groups = null): ValidationResult
    {
        return $this->validator->validate($value, $constraints, $groups);
    }

    /**
     * @return ObjectManager
     */
    public function getEntityManager(): ObjectManager
    {
        return $this->getDoctrine()->getManager();
    }

    /**
     * @param $data
     * @param ErrorsIterator|null $errorsIterator
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function response($data, ErrorsIterator $errorsIterator = null)
    {
        $preparedData = $this->apiResponseService->getPreparedData();
        $status = 200;
        $msg = 'Success';
        if (null !== $errorsIterator && 0 !== $errorsIterator->count()){
            $status = 422;
            $msg = 'Error';
            $preparedData->setErrors($errorsIterator);
        }

        $preparedData
            ->setData($data)
            ->setStatus($status)
            ->setMsg($msg);
        return $this->apiResponseService->response($preparedData);
    }

    /**
     * @param null $data
     * @param ConstraintViolationListInterface|null $violationList
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function responseWithValidation($data = null, ConstraintViolationListInterface $violationList = null)
    {
        $preparedData = $this->apiResponseService->getPreparedData();
        $status = 200;
        $msg = 'Success';
        if (null !== $violationList && 0 !== $violationList->count()){
            $status = 422;
            $msg = 'Error';
            $this->apiResponseService->loadErrorsFromConstraints($preparedData, $violationList);
        }

        $preparedData
            ->setData($data)
            ->setStatus($status)
            ->setMsg($msg);
        return $this->apiResponseService->response($preparedData);
    }

    /**
     * @param $data
     * @param PaginationSettingsInterface|null $paginationSettings
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function responseList($data, ?PaginationSettingsInterface $paginationSettings=null)
    {
        $responseData = [
            'rows' => $data,
            'count' => count($data),
            'pages' => 1,
        ];
        if (null !== $paginationSettings){
            $responseData['count'] = $paginationSettings->getItemsCount();
            $responseData['pages'] = $this->paginationService->getPagination($paginationSettings)->pagesCount();
        }
        return $this->response($responseData);
    }

}