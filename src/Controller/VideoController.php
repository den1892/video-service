<?php


namespace App\Controller;


use App\Entity\Video;
use App\InputHandling\CustomObjectData\VideoFilterObjectData;
use App\InputHandling\Serializer\VideoSerializer;
use App\Pagination\Pagination;
use App\Repository\Video\VideoFilterData;
use App\Repository\Video\VideoRepositoryInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/video")
 * Class VideoController
 * @package App\Controller
 *
 */
class VideoController extends ApiBaseControllerAbstract
{

    /**
     * @Route("/{id}", methods={"GET"})
     * @param Video $video
     * @param VideoSerializer $videoSerializer
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function view(Video $video, VideoSerializer $videoSerializer)
    {
        $normalizingData = $videoSerializer->normalize($video);
        return $this->response($normalizingData);
    }

    /**
     * @Route("", methods={"GET"})
     * @param VideoSerializer $videoSerializer
     * @param VideoFilterObjectData $filterObjectData
     * @param VideoRepositoryInterface $videoRepository
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function list(VideoSerializer $videoSerializer, VideoFilterObjectData $filterObjectData, VideoRepositoryInterface $videoRepository)
    {
        $filterData = new VideoFilterData();

        $this->denormalizeRequestQuery($filterObjectData);

        $validationResult = $this->validate($filterObjectData);

        if ($validationResult->isValid()){
            $filterObjectData->loadFilterData($filterData);
        } else {
            return $this->responseWithValidation([], $validationResult->getConstraintViolationList());
        }

        $paginationSettings = $this->getPaginationSettings();
        $videoList = $videoRepository->getList($paginationSettings, $filterData);
        $normalizingData = $videoSerializer->normalize($videoList);

        return $this->responseList($normalizingData, $paginationSettings);

    }

}