<?php


namespace App\Controller\User;


use App\Controller\ApiBaseControllerAbstract;
use App\Entity\Video;
use App\InputHandling\CustomObjectData\VideoSaveObjectData;
use App\InputHandling\Serializer\VideoSerializer;
use App\Repository\Video\VideoFilterData;
use App\Repository\Video\VideoRepositoryInterface;
use App\Repository\VideoRepository;
use App\User\AuthUser\AuthUserInterface;
use App\Video\VideoManager;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/video")
 * Class VideoController
 * @package App\Controller\User
 */
class VideoController extends ApiBaseControllerAbstract
{

    /**
     * @Route("", methods={"POST"})
     * @param VideoSaveObjectData $videoSaveObjectData
     * @param VideoManager $videoManager
     * @param VideoSerializer $videoSerializer
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function save(VideoSaveObjectData $videoSaveObjectData, VideoManager $videoManager, VideoSerializer $videoSerializer)
    {

        $video = new Video();

        $this->denormalizeRequest(VideoSaveObjectData::class, $videoSaveObjectData);
        $this->denormalizeRequestFile(VideoSaveObjectData::class, $videoSaveObjectData);
        $resultData = [];

        $validationResult = $this->validate($videoSaveObjectData);
        if ($validationResult->isValid()){

           $videoSaveObjectData->loadVideo($video);
           $videoManager->saveNewVideo($video, $videoSaveObjectData->getVideoFile());
           $resultData = $videoSerializer->normalize($video);
        }

        return $this->responseWithValidation($resultData, $validationResult->getConstraintViolationList());
    }

    /**
     * @Route("/{id}/{type<like|dislike>}", methods="POST")
     * @param Video $video
     * @param VideoManager $videoManager
     * @param VideoSerializer $videoSerializer
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function like(string $type, Video $video, VideoManager $videoManager, VideoSerializer $videoSerializer)
    {
        if ('like' === $type) {
            $videoManager->like($video);
        } else {
            $videoManager->dislike($video);
        }
        $data = $videoSerializer->normalize($video);
        return $this->response($data);
    }

//    public function list(VideoSerializer $serializer, VideoRepositoryInterface $videoRepository, AuthUserInterface $authUser)
//    {
//        $filterData = new VideoFilterData();
//        $filterData->setUserLiked($authUser->currentUser());
//
//        $paginationSettings = $this->getPaginationSettings();
//
//        $videos = $videoRepository->getList($paginationSettings, $filterData);
//
//        $normalizeData = $serializer->normalize($videos);
//
//        return $this->responseList($normalizeData, $paginationSettings);
//    }

}