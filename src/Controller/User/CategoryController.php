<?php


namespace App\Controller\User;


use App\Controller\ApiBaseControllerAbstract;
use App\Entity\Category;
use App\InputHandling\CustomObjectData\CategorySaveObjectData;
use App\InputHandling\Serializer\CategorySerializer;
use App\InputHandling\ArrayValidator\CategoryFilterDataValidation;
use App\InputHandling\ArrayValidator\CategorySaveValidation;
use App\Repository\Category\CategoryRepositoryInterface;
use App\Repository\Category\CategoryFilterData;
use App\User\AuthUser\AuthUserInterface;
use App\Video\VideoManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/category")
 * Class CategoryController
 * @package App\Controller\User
 */
class CategoryController extends ApiBaseControllerAbstract
{

    /**
     * @Route("", methods={"POST"})
     * @param CategorySaveObjectData $categorySaveObjectData
     * @param CategorySerializer $categorySerializer
     * @param VideoManager $videoManager
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function save(CategorySaveObjectData $categorySaveObjectData, CategorySerializer $categorySerializer,  VideoManager $videoManager)
    {
         $category = new Category();
         $resultData = [];

         $this->denormalizeRequest($categorySaveObjectData);

         $validationResult = $this->validate($categorySaveObjectData);
         if ($validationResult->isValid()){

             $categorySaveObjectData->loadCategory($category);
             $videoManager->saveNewVideoCategory($category);
             $resultData = $categorySerializer->normalize($category);

         }

         return $this->responseWithValidation($resultData, $validationResult->getConstraintViolationList());
    }

}