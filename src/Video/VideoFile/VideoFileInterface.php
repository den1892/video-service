<?php


namespace App\Video\VideoFile;


use App\Entity\Video;
use Symfony\Component\HttpFoundation\File\File;

interface VideoFileInterface
{

    /**
     * @param Video $video
     * @param File $file
     * @return mixed
     */
    public function saveFile(Video $video, File $file);

    /**
     * @param Video $video
     * @return mixed
     */
    public function removeFile(Video $video);

}