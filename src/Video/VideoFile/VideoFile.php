<?php


namespace App\Video\VideoFile;


use App\Entity\Video;
use FFMpeg\Coordinate\TimeCode;
use FFMpeg\FFMpeg;
use Symfony\Component\HttpFoundation\File\File;

class VideoFile implements VideoFileInterface
{


    /**
     * @var string
     */
    private $publicPath;
    /**
     * @var string
     */
    private $fullPathToPublic;
    /**
     * @var FFMpeg
     */
    private $FFMpeg;

    /**
     * VideoFileSave constructor.
     * @param string $publicPath
     * @param string $fullPathToPublic
     * @param FFMpeg $FFMpeg
     *
     *  fullPathToPublic = /var/www/site/public
     *  publicPath = video
     *  fullPath = fullPathToPublic.'/'.publicPath
     */
    public function __construct(string $publicPath, string $fullPathToPublic, FFMpeg $FFMpeg = null)
    {
        $this->publicPath = $publicPath;
        $this->fullPathToPublic = $fullPathToPublic;
        $this->FFMpeg = $FFMpeg;
    }

    /**
     * @param Video $video
     * @param File $file
     */
    public function saveFile(Video $video, File $file)
    {
        $name = $this->fileName($file);
        $file->move($this->fullPath(), $name);

        $videoFFMpeg = $this->FFMpeg->open($this->fullPath().'/'.$name);

        $videoFFMpeg->frame(TimeCode::fromSeconds(1))->save($this->fullPath().'/avatar/'.$name.'.jpg');

        $video->setFile($this->publicPath.'/'.$name);
        $video->setAvatar($this->publicPath.'/avatar/'.$name.'.jpg');
    }

    /**
     * @param Video $video
     */
    public function removeFile(Video $video)
    {
        $file = $this->fullPath($video);
        if (is_file($file)){
            unlink($file);
        }
    }

    /**
     * @param File $file
     * @return string
     */
    private function fileName(File $file): string
    {
        return  date('Y-m-d_H-i-s').'_'.uniqid().'.'.$file->guessExtension();
    }

    private function fullPath(Video $video = null)
    {
        if (null !== $video){
            $publicPath = $video->getFile();
        } else {
            $publicPath = $this->publicPath;
        }
        return $this->fullPathToPublic.'/'.$publicPath;
    }

}