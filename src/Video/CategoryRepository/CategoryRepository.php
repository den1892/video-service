<?php


namespace App\Video\CategoryRepository;


use App\Entity\Category;
use App\User\AuthUser\AuthUserInterface;
use \App\Repository\Category\CategoryRepositoryInterface as CategoryRepositoryInterfaceAlias;

class CategoryRepository implements CategoryRepositoryInterface
{

    /**
     * @var AuthUserInterface
     */
    private $authUser;
    /**
     * @var CategoryRepositoryInterfaceAlias
     */
    private $categoryRepository;

    public function __construct(AuthUserInterface $authUser, CategoryRepositoryInterfaceAlias $categoryRepository)
    {
        $this->authUser = $authUser;
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * @param Category $category
     */
    public function saveNewCategory(Category $category): void
    {
        $category->setCreator($this->authUser->currentUser());
        $this->categoryRepository->save($category);
    }
}