<?php


namespace App\Video\CategoryRepository;


use App\Entity\Category;

interface CategoryRepositoryInterface
{

    /**
     * @param Category $category
     */
    public function saveNewCategory(Category $category): void;

}