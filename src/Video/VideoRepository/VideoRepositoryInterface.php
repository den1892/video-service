<?php


namespace App\Video\VideoRepository;


use App\Entity\Video;

interface VideoRepositoryInterface
{

    /**
     * @param Video $video
     */
    public function saveNewVideo(Video $video): void;

}