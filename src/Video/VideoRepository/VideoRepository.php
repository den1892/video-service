<?php


namespace App\Video\VideoRepository;


use App\Entity\Video;
use App\User\AuthUser\AuthUserInterface;
use App\Repository\Video\VideoRepositoryInterface as VideoRepositoryInterfaceAlias;

class VideoRepository implements VideoRepositoryInterface
{


    /**
     * @var AuthUserInterface
     */
    private $authUser;
    /**
     * @var VideoRepositoryInterfaceAlias
     */
    private $videoRepository;

    public function __construct(AuthUserInterface $authUser, VideoRepositoryInterfaceAlias $videoRepository)
    {
        $this->authUser = $authUser;
        $this->videoRepository = $videoRepository;
    }

    /**
     * @param Video $video
     */
    public function saveNewVideo(Video $video): void
    {
        $video->setOwner($this->authUser->currentUser());
        $this->videoRepository->save($video);
    }
}