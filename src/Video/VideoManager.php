<?php


namespace App\Video;




use App\Entity\Category;
use App\Entity\Video;
use App\Video\CategoryRepository\CategoryRepositoryInterface;
use App\Video\Like\LikeInterface;
use App\Video\VideoFile\VideoFileInterface;
use App\Video\VideoRepository\VideoRepositoryInterface;
use Symfony\Component\HttpFoundation\File\File;

class VideoManager
{


    /**
     * @var CategoryRepositoryInterface
     */
    private $categoryRepository;
    /**
     * @var LikeInterface
     */
    private $like;
    /**
     * @var VideoFileInterface
     */
    private $videoFile;
    /**
     * @var VideoRepositoryInterface
     */
    private $videoRepository;

    public function __construct(CategoryRepositoryInterface $categoryRepository, LikeInterface $like, VideoFileInterface $videoFile, VideoRepositoryInterface $videoRepository)
    {

        $this->categoryRepository = $categoryRepository;
        $this->like = $like;
        $this->videoFile = $videoFile;
        $this->videoRepository = $videoRepository;
    }


    /**
     * @param Video $video
     * @param File $file
     */
    public function saveNewVideo(Video $video, File $file): void
    {
        $this->videoFile->saveFile($video, $file);
        $this->videoRepository->saveNewVideo($video);
    }

    /**
     * @param Category $category
     */
    public function saveNewVideoCategory(Category $category): void
    {
        $this->categoryRepository->saveNewCategory($category);
    }

    /**
     * @param Video $video
     */
    public function like(Video $video): void
    {
        $this->like->likeVideo($video);
    }

    /**
     * @param Video $video
     */
    public function dislike(Video $video): void
    {
        $this->like->disLikeVideo($video);
    }
}