<?php


namespace App\Video\Like;


use App\Entity\VideoLike;
use App\Entity\User;
use App\Entity\Video;
use App\Repository\VideoLikeRepository;
use App\User\AuthUser\AuthUserInterface;
use Doctrine\ORM\EntityManagerInterface;

class Like implements LikeInterface
{

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var VideoLikeRepository
     */
    private $likeRepository;

    /**
     * @var User
     */
    private $user;

    public function __construct(EntityManagerInterface $entityManager, VideoLikeRepository $likeRepository, AuthUserInterface $user)
    {
        $this->entityManager = $entityManager;
        $this->likeRepository = $likeRepository;
        $this->user = $user->currentUser();
    }

    /**
     * @param Video $video
     * @param User $user
     */
    public function likeVideo(Video $video)
    {
        $like = $this->likeEntity($video, $this->user);
        $like->setDisLike(false);

        $this->entityManager->persist($like);
        $this->entityManager->flush();
    }

    /**
     * @param Video $video
     * @param User $user
     */
    public function disLikeVideo(Video $video)
    {
        $like = $this->likeEntity($video, $this->user);
        $like->setDisLike(true);

        $this->entityManager->persist($like);
        $this->entityManager->flush();
    }

    /**
     * @param Video $video
     * @param User $user
     * @return VideoLike
     */
    private function likeEntity(Video $video, User $user): VideoLike
    {
        $like = $this->likeRepository->findOneBy(['userID'=>$user->getID(), 'videoID' => $video->getId()]);

        if (null === $like){
            $like = (new VideoLike())
                ->setVideo($video)
                ->setUser($user)
                ;
        }

        return $like;
    }

}