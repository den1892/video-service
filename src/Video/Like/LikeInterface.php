<?php


namespace App\Video\Like;


use App\Entity\Video;

interface LikeInterface
{

    /**
     * @param Video $video
     * @return mixed
     */
    public function likeVideo(Video $video);

    /**
     * @param Video $video
     * @return mixed
     */
    public function disLikeVideo(Video $video);

}