<?php


namespace App\EntityHelper;


use App\EntityHelper\AssociationLoad\AssociationIterator;
use App\EntityHelper\AssociationLoad\EntityAssociationLoad;
use App\EntityHelper\AssociationLoad\Interfaces\AssociationIteratorInterface;

abstract class AssociationLoadAbstract
{

    /**
     * @var AssociationIteratorInterface
     */
    private $associationsOneToOne = [];
    /**
     * @var EntityAssociationLoad
     */
    private $entityAssociationLoad;

    /**
     * FirmAssociationLoad constructor.
     * @param EntityAssociationLoad $entityAssociationLoad
     */
    public function __construct(EntityAssociationLoad $entityAssociationLoad)
    {
        $this->associationsOneToOne = new AssociationIterator();
        $this->entityAssociationLoad = $entityAssociationLoad;
    }

    /**
     * @param string $joinColumnName
     * @param string $associationFieldName
     * @param string $targetEntity
     * @return AssociationLoadAbstract
     */
    protected function addAssociation(string $joinColumnName, string $associationFieldName, string $targetEntity): self
    {
        $this->associationsOneToOne->addAssociation($joinColumnName, $associationFieldName, $targetEntity);
        return $this;
    }

    abstract protected function addAssociations();

    /**
     * @param $entity
     */
    public function loadOneToOne($entity)
    {
        $this->entityAssociationLoad->loadOneToOne($entity, $this->associationsOneToOne);
    }

}