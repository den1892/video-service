<?php


namespace App\EntityHelper\AssociationLoad;


use App\EntityHelper\AssociationLoad\Interfaces\AssociationItemInterface;

class AssociationItem implements AssociationItemInterface
{

    /**
     * @var string
     */
    private $joinColumnName;
    /**
     * @var string
     */
    private $associationFieldName;
    /**
     * @var string
     */
    private $targetEntity;

    public function __construct(string $joinColumnName, string $associationFieldName, string $targetEntity)
    {
        $this->joinColumnName = $joinColumnName;
        $this->associationFieldName = $associationFieldName;
        $this->targetEntity = $targetEntity;
    }

    /**
     * @return string
     */
    public function getJoinColumnName(): string
    {
        return $this->joinColumnName;
    }

    /**
     * @return string
     */
    public function getAssociationFieldName(): string
    {
        return $this->associationFieldName;
    }

    /**
     * @return string
     */
    public function getTargetEntity(): string
    {
        return $this->targetEntity;
    }


}