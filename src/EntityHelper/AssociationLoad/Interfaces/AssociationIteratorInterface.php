<?php


namespace App\EntityHelper\AssociationLoad\Interfaces;


interface AssociationIteratorInterface extends \Iterator
{

    public function current(): AssociationItemInterface;

}