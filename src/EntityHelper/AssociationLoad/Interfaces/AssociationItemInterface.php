<?php


namespace App\EntityHelper\AssociationLoad\Interfaces;


interface AssociationItemInterface
{

    public  function getJoinColumnName(): string;

    public function getAssociationFieldName(): string;

    public function getTargetEntity(): string;

}