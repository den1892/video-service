<?php


namespace App\EntityHelper\AssociationLoad;


use App\EntityHelper\AssociationLoad\Interfaces\AssociationItemInterface;
use App\EntityHelper\AssociationLoad\Interfaces\AssociationIteratorInterface;

class AssociationIterator extends \ArrayIterator implements AssociationIteratorInterface
{

    /**
     * @return AssociationItemInterface
     */
    public function current(): AssociationItemInterface
    {
       return parent::current();
    }

    /**
     * @param string $joinColumnName
     * @param string $associationFieldName
     * @param string $targetEntity
     */
    public function addAssociation(string $joinColumnName, string $associationFieldName, string $targetEntity)
    {
        $associationItem = new AssociationItem($joinColumnName, $associationFieldName, $targetEntity);
        parent::append($associationItem);
    }

}