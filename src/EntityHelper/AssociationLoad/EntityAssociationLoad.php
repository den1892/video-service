<?php


namespace App\EntityHelper\AssociationLoad;


use App\EntityHelper\AssociationLoad\Interfaces\AssociationIteratorInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\PropertyAccess\PropertyAccessor;

class EntityAssociationLoad
{

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var PropertyAccessor
     */
    private $propertyAccessor;


    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->propertyAccessor = new PropertyAccessor();
    }

    /**
     * @param $entity
     * @param AssociationIteratorInterface $associationIterator
     */
    public function loadOneToOne($entity, AssociationIteratorInterface $associationIterator)
    {

        foreach ($associationIterator as $associations){
            $joinPropertyValue = $this->propertyAccessor->getValue($entity, $associations->getJoinColumnName());
            if (null === $joinPropertyValue){
                continue;
            }
            $targetEntityInstance = $this->entityManager->getRepository($associations->getTargetEntity())->find($joinPropertyValue);
            if (null !== $targetEntityInstance){
                $this->propertyAccessor->setValue($entity, $associations->getAssociationFieldName(), $targetEntityInstance);
            }
        }
    }

}