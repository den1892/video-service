<?php


namespace App\InputHandling\CustomObjectData;


use App\Entity\Category;
use App\Repository\Category\CategoryRepositoryInterface;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\Validator\Mapping\ClassMetadata;

class CategorySaveObjectData implements CustomObjectDataInterface
{

    private $name;
    /**
     * @var CategoryRepositoryInterface
     */
    private $categoryRepository;

    public function __construct(CategoryRepositoryInterface $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata
            ->addPropertyConstraints('name', [
                new Length(['max'=>255]),
                new NotBlank()
            ])
            ->addConstraint(new Callback('validateExistingCategoryByName'))
        ;
    }

    /**
     * @param ExecutionContextInterface $context
     */
    public function validateExistingCategoryByName(ExecutionContextInterface $context){
        $category = $this->categoryRepository->findByName($this->name);
        if (null !== $category){
            $context
                ->buildViolation('Category with this name already exists!')
                ->addViolation()
            ;
        }
    }
    public function loadCategory(Category $category): void
    {
        $category->setName($this->name);
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }


}