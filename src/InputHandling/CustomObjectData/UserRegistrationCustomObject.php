<?php


namespace App\InputHandling\CustomObjectData;


use App\Entity\User;
use App\Repository\User\UserRepositoryInterface;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\Validator\Mapping\ClassMetadata;

class UserRegistrationCustomObject implements CustomObjectDataInterface
{

    private $email;

    private $firstName;

    private $lastName;

    private $password;
    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;


    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public static function loadValidatorMetadata(ClassMetadata $metadata): void
    {
        $metadata
            ->addPropertyConstraints('email', [new Email(), new NotBlank()])
            ->addPropertyConstraint('firstName', new Length(['max'=>255]))
            ->addPropertyConstraint('lastName', new Length(['max'=>255]))
            ->addPropertyConstraints('password', [new Length(['min'=>6]), new NotBlank()])
            ->addConstraint(new Callback('validateUniqueUser'))
            ;
    }

    /**
     * @param ExecutionContextInterface $context
     */
    public function validateUniqueUser(ExecutionContextInterface $context)
    {
        if (null === $this->email){
            return;
        }
        $user = $this->userRepository->findByEmail($this->email);
        if (null !== $user){
            $context->buildViolation('User with this email already exists!')->addViolation();
        }
    }

    /**
     * @param User $user
     */
    public function loadUser(User $user): void
    {
        $user
            ->setFirstName($this->firstName)
            ->setLastName($this->lastName)
            ->setEmail($this->email)
        ;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email): void
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param mixed $firstName
     */
    public function setFirstName($firstName): void
    {
        $this->firstName = $firstName;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param mixed $lastName
     */
    public function setLastName($lastName): void
    {
        $this->lastName = $lastName;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password): void
    {
        $this->password = $password;
    }


}