<?php


namespace App\InputHandling\CustomObjectData;


use App\Entity\Category;
use App\Entity\Video;
use App\Repository\Category\CategoryRepositoryInterface;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Mapping\ClassMetadata;

class VideoSaveObjectData implements CustomObjectDataInterface
{

    private $name;

    private $description;

    private $categoryID;

    private $videoFile;
    /**
     * @var CategoryRepositoryInterface
     */
    private $categoryRepository;


    public function __construct(CategoryRepositoryInterface $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }


    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata
            ->addPropertyConstraints('name', [new Length(['max'=>255]), new NotBlank(['message' => 'Enter video title'])])
            ->addPropertyConstraint('description', new Length(['max'=>65535]))
            ->addPropertyConstraint('categoryID', new NotBlank(['message' => 'Select a category']))
            ->addPropertyConstraints('videoFile', [
                new File([
                    'mimeTypes' => ['video/mp4', 'video/3gpp'],
                    'maxSize' => '50M'
                ]),
                new NotBlank()])
            ->addGetterConstraint('category', new NotNull(['message' => 'Select a category']));
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description): void
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getCategoryID()
    {
        return $this->categoryID;
    }

    /**
     * @param mixed $categoryID
     */
    public function setCategoryID($categoryID): void
    {
        $this->categoryID = $categoryID;
    }

    /**
     * @return mixed
     */
    public function getVideoFile()
    {
        return $this->videoFile;
    }

    /**
     * @param mixed $videoFile
     */
    public function setVideoFile($videoFile): void
    {
        $this->videoFile = $videoFile;
    }

    /**
     * @return Category|null
     */
    public function getCategory(): ?Category
    {
        if (!is_scalar($this->categoryID)){
            return null;
        }
        return $this->categoryRepository->findByID((int)$this->categoryID);
    }

    public function loadVideo(Video $video)
    {
        $video
            ->setName($this->name)
            ->setCategory($this->getCategory())
            ->setDescription($this->description);
    }

}