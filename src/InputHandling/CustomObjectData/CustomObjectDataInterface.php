<?php


namespace App\InputHandling\CustomObjectData;


use Symfony\Component\Validator\Mapping\ClassMetadata;

interface CustomObjectDataInterface
{

    const TAG_NAME = 'app.custom_object_data';

    public static function loadValidatorMetadata(ClassMetadata $metadata);

}