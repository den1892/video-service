<?php


namespace App\InputHandling\CustomObjectData;


use App\Repository\Category\CategoryFilterData;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Mapping\ClassMetadata;

class CategoryFilterObjectData implements CustomObjectDataInterface
{

    /**
     * @var string|null
     */
    private $name;

    public function getName()
    {
        return $this->name;
    }

    public function setName($name): void
    {
        $this->name = $name;
    }

    public function loadFilterData(CategoryFilterData $filterData): void
    {
        $filterData->setName($this->name);
    }

    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addPropertyConstraint('name', new Length(['max'=>255]));
    }
}