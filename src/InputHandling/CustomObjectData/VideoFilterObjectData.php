<?php


namespace App\InputHandling\CustomObjectData;


use App\Entity\Category;
use App\Entity\User;
use App\Repository\Category\CategoryRepositoryInterface;
use App\Repository\User\UserRepositoryInterface;
use App\Repository\Video\VideoFilterData;
use App\User\AuthUser\AuthUserInterface;
use App\Validator\Constraints\ExistingEntity;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Validator\Constraints\Choice;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\Type;
use Symfony\Component\Validator\Mapping\ClassMetadata;

class VideoFilterObjectData implements CustomObjectDataInterface
{

    private $categoryID;

    private $searchString;

    private $authUserLiked;

    private $authUserOwner;

    private $authUserDisliked;

    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;
    /**
     * @var CategoryRepositoryInterface
     */
    private $categoryRepository;
    /**
     * @var AuthUserInterface
     */
    private $authUser;

    public function __construct(UserRepositoryInterface $userRepository, CategoryRepositoryInterface $categoryRepository, AuthUserInterface $authUser)
    {
        $this->userRepository = $userRepository;
        $this->categoryRepository = $categoryRepository;
        $this->authUser = $authUser;
    }

    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata
            ->addPropertyConstraint('categoryID', new ExistingEntity(['propertyName' => 'id', 'entityClass' => Category::class]))
            ->addPropertyConstraint('searchString', new Length(['max'=>255]))
            ->addPropertyConstraint('authUserOwner',  new Choice(['choices' => ['0','1']]))
            ->addPropertyConstraint('authUserLiked', new Choice(['choices' => ['0','1']]))
            ->addPropertyConstraint('authUserDisliked',  new Choice(['choices' => ['0','1']]))
            ;
    }

    /**
     * @param VideoFilterData $filterData
     */
    public function loadFilterData(VideoFilterData $filterData): void
    {
       // $user = $this->userRepository->findByID($this->userID);
        $category = $this->categoryRepository->findByID($this->categoryID);

        $filterData->setCategory($category);
        $filterData->setSearchString($this->searchString);

        if (1 == $this->authUserLiked){
            $filterData->setUserLiked($this->getAuthUser());
        }
        if (1 == $this->authUserDisliked){
            $filterData->setUserDisliked($this->getAuthUser());
        }
        if (1 == $this->authUserOwner){
            $filterData->setUser($this->getAuthUser());
        }

    }

    /**
     * @return VideoFilterData
     */
    public function createFilterData(): VideoFilterData
    {
        $filterData = new VideoFilterData();
        $this->loadFilterData($filterData);
        return $filterData;
    }

    private function getAuthUser(): User
    {
        $user = $this->authUser->currentUser();
        if (null === $user){
            throw new AuthenticationException();
        }
        return $user;
    }

    /**
     * @return mixed
     */
    public function getCategoryID()
    {
        return $this->categoryID;
    }

    /**
     * @param mixed $categoryID
     */
    public function setCategoryID($categoryID): void
    {
        $this->categoryID = $categoryID;
    }

    /**
     * @return mixed
     */
    public function getSearchString()
    {
        return $this->searchString;
    }

    /**
     * @param mixed $searchString
     */
    public function setSearchString($searchString): void
    {
        $this->searchString = $searchString;
    }

    /**
     * @return mixed
     */
    public function getAuthUserLiked()
    {
        return $this->authUserLiked;
    }

    /**
     * @param mixed $authUserLiked
     */
    public function setAuthUserLiked($authUserLiked): void
    {
        $this->authUserLiked = $authUserLiked;
    }

    /**
     * @return mixed
     */
    public function getAuthUserOwner()
    {
        return $this->authUserOwner;
    }

    /**
     * @param mixed $authUserOwner
     */
    public function setAuthUserOwner($authUserOwner): void
    {
        $this->authUserOwner = $authUserOwner;
    }

    /**
     * @return mixed
     */
    public function getAuthUserDisliked()
    {
        return $this->authUserDisliked;
    }

    /**
     * @param mixed $authUserDisliked
     */
    public function setAuthUserDisliked($authUserDisliked): void
    {
        $this->authUserDisliked = $authUserDisliked;
    }
}