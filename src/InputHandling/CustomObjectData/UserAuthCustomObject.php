<?php


namespace App\InputHandling\CustomObjectData;


use App\Entity\User;
use App\User\UserManager;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\Validator\Mapping\ClassMetadata;

class UserAuthCustomObject implements CustomObjectDataInterface
{

    private $email;

    private $password;
    /**
     * @var UserManager
     */
    private $userManager;

    public function __construct(UserManager $userManager)
    {
        $this->userManager = $userManager;
    }

    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata
            ->addPropertyConstraints('email', [new Email(), new NotBlank()])
            ->addPropertyConstraints('password', [new Length(['min'=>6]), new NotBlank()])
            ->addConstraint(new Callback('validateUserExisting'))
        ;
    }

    /**
     * @param ExecutionContextInterface $context
     */
    public function validateUserExisting(ExecutionContextInterface $context)
    {
        if (null === $this->email | null === $this->password){
            return;
        }
        if (null === $this->findUser()){
            $context->buildViolation('User with such email or password not found!')->addViolation();
        }
    }

    public function findUser(): ?User
    {
        if (null === $this->email | null === $this->password){
            return null;
        }
        return $this->userManager->findUserByAuth($this->email, $this->password);
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email): void
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password): void
    {
        $this->password = $password;
    }

}