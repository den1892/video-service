<?php


namespace App\InputHandling\Serializer;


use App\Serializer\ContextBuilder;
use App\Serializer\SerializerBuilder;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Serializer;

abstract class EntitySerializerAbstract
{

    /**
     * @var Serializer
     */
    private $serializerForNormalizing;

    /**
     * @var array
     */
    private $contextForNormalizing=[];

    /**
     * @var Serializer
     */
    private $serializerForDenormalizing;

    /**
     * @var array
     */
    private $contextForDenormalizing=[];


    private function bootForNormalizing()
    {
        if (null === $this->serializerForNormalizing){
            $serializerBulder = new SerializerBuilder();
            $contextBuilder = new ContextBuilder();
            $this->configureNormalizing($serializerBulder, $contextBuilder);
            $this->serializerForNormalizing = $serializerBulder->getSerializer();
            $this->contextForNormalizing = $contextBuilder->getContext();
        }
    }

    private function bootForDenormalizing()
    {
        if (null === $this->serializerForDenormalizing){
            $serializerBulder = new SerializerBuilder();
            $contextBuilder = new ContextBuilder();
            $this->configureDenormalizing($serializerBulder, $contextBuilder);
            $this->serializerForDenormalizing = $serializerBulder->getSerializer();
            $this->contextForDenormalizing = $contextBuilder->getContext();
        }
    }

    /**
     * @param $data
     * @return array|bool|float|int|mixed|string
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function normalize($data)
    {
        $this->bootForNormalizing();
        return  $this->serializerForNormalizing->normalize($data, null, $this->contextForNormalizing);
    }

    /**
     * @param $data
     * @param $entityClass
     * @param null $existingData
     * @return object
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function denormalize($data, $entityClass, $existingData = null)
    {

        if (is_object($entityClass)){
            $existingData = $entityClass;
            $entityClass = get_class($existingData);
        }

        $this->bootForDenormalizing();

        $context = $this->contextForDenormalizing;
        $context[AbstractNormalizer::OBJECT_TO_POPULATE] = $existingData;

        return $this->serializerForDenormalizing->denormalize($data, $entityClass, null, $context);
    }

    /**
     * @param Request $request
     * @param $entityClass
     * @param null $existingData
     * @return object
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function denormalizeRequest(Request $request, $entityClass, $existingData = null)
    {
        return $this->denormalize($request->request->all(), $entityClass, $existingData);
    }

    /**
     * @param Request $request
     * @param $entityClass
     * @param null $existingData
     * @return object
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function denormalizeRequestQuery(Request $request, $entityClass, $existingData = null)
    {
        return $this->denormalize($request->query->all(), $entityClass, $existingData);
    }

    /**
     * @param Request $request
     * @param $entityClass
     * @param null $existingData
     * @return object
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function denormalizeRequestFile(Request $request, $entityClass, $existingData = null)
    {
        return $this->denormalize($request->files->all(), $entityClass, $existingData);
    }

    /**
     * @param SerializerBuilder $serializerBuilder
     * @param ContextBuilder $contextBuilder
     * @return mixed
     */
    abstract protected function configureNormalizing(SerializerBuilder $serializerBuilder, ContextBuilder $contextBuilder);

    /**
     * @param SerializerBuilder $serializerBuilder
     * @param ContextBuilder $contextBuilder
     * @return mixed
     */
    abstract protected function configureDenormalizing(SerializerBuilder $serializerBuilder, ContextBuilder $contextBuilder);
}