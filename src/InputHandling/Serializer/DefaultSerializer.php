<?php


namespace App\InputHandling\Serializer;


use App\Serializer\ContextBuilder;
use App\Serializer\SerializerBuilder;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;

class DefaultSerializer extends EntitySerializerAbstract
{

    /**
     * @param SerializerBuilder $serializerBuilder
     * @param ContextBuilder $contextBuilder
     * @return mixed
     */
    protected function configureNormalizing(SerializerBuilder $serializerBuilder, ContextBuilder $contextBuilder)
    {
        // TODO: Implement configureNormalizing() method.
    }

    /**
     * @param SerializerBuilder $serializerBuilder
     * @param ContextBuilder $contextBuilder
     * @return mixed
     */
    protected function configureDenormalizing(SerializerBuilder $serializerBuilder, ContextBuilder $contextBuilder)
    {
        $serializerBuilder->addNormalizer(new ArrayDenormalizer());
    }
}