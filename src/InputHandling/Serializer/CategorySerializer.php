<?php


namespace App\InputHandling\Serializer;


use App\InputHandling\Serializer\EntitySerializerAbstract;
use App\Serializer\ContextBuilder;
use App\Serializer\SerializerBuilder;

class CategorySerializer extends EntitySerializerAbstract
{

    /**
     * @param SerializerBuilder $serializerBuilder
     * @param ContextBuilder $contextBuilder
     * @return mixed
     */
    protected function configureNormalizing(SerializerBuilder $serializerBuilder, ContextBuilder $contextBuilder)
    {
       $contextBuilder->setAttributes(['name', 'id']);
    }

    /**
     * @param SerializerBuilder $serializerBuilder
     * @param ContextBuilder $contextBuilder
     * @return mixed
     */
    protected function configureDenormalizing(SerializerBuilder $serializerBuilder, ContextBuilder $contextBuilder)
    {
        $contextBuilder->setAttributes(['name']);
    }
}