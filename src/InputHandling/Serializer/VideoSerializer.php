<?php


namespace App\InputHandling\Serializer;


use App\InputHandling\Serializer\EntitySerializerAbstract;
use App\Serializer\ContextBuilder;
use App\Serializer\SerializerBuilder;
use Symfony\Component\DependencyInjection\ContainerInterface;

class VideoSerializer extends EntitySerializerAbstract
{


    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(ContainerInterface $container)
    {

        $this->container = $container;
    }

    /**
     * @param SerializerBuilder $serializerBuilder
     * @param ContextBuilder $contextBuilder
     * @return mixed
     */
    protected function configureNormalizing(SerializerBuilder $serializerBuilder, ContextBuilder $contextBuilder)
    {
        $contextBuilder->setAttributes([
            'name', 'description', 'file', 'avatar', 'likesCount', 'dislikesCount', 'id', 'authUserLiked', 'authUserDisliked',
            'category' => ['name', 'description', 'id'],
            'owner' => ['firstName', 'lastName'],
//            'likesList' => ['user' => ['email', 'firstName', 'lastName']],
//            'dislikesList' => ['user' => ['email','firstName', 'lastName']],
        ]);
        $contextBuilder->setCallbacks([
            'file' => function ($value) {
                if (null === $value) {
                    return null;
                }
                return $this->container->getParameter('app.site_url') . '/' . $value;
            },
            'avatar' => function ($value) {
                if (null === $value) {
                    return null;
                }
                return $this->container->getParameter('app.site_url') . '/' . $value;
            }
        ]);
    }

    /**
     * @param SerializerBuilder $serializerBuilder
     * @param ContextBuilder $contextBuilder
     * @return mixed
     */
    protected function configureDenormalizing(SerializerBuilder $serializerBuilder, ContextBuilder $contextBuilder)
    {
        $contextBuilder->setAttributes([
            'name', 'description'
        ]);
    }
}