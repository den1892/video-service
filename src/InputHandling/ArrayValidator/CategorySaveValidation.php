<?php


namespace App\InputHandling\ArrayValidator;


use App\Entity\Category;
use App\InputHandling\ArrayValidator\ValidatorAbstract;
use App\Repository\CategoryRepository;
use App\Validator\Validator;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class CategorySaveValidation extends ValidatorAbstract
{

    /**
     * @var CategoryRepository
     */
    private $categoryRepository;

    public function __construct(CategoryRepository $categoryRepository, Validator $validator)
    {
        parent::__construct($validator);
        $this->categoryRepository = $categoryRepository;
    }

    public function constraints(): ?Constraint
    {
        return new Collection([
            'name' => [
                new Length(['max' => 255]),
                new NotBlank(),
                new Callback([
                    'callback' => function ($value, ExecutionContextInterface $context) {
                        if (null !== $this->categoryRepository->findOneBy(['name' => $value])) {
                            $context->buildViolation('A category with this name already exists')->addViolation();
                        }
                    }
                ])
            ]
        ]);
    }

}