<?php


namespace App\InputHandling\ArrayValidator;


use App\Entity\Category;
use App\InputHandling\ArrayValidator\ValidatorAbstract;
use App\Validator\Constraints\ExistingEntity;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class VideoSaveValidation extends ValidatorAbstract
{

    public function constraints(): ?Constraint
    {
        return new Collection([
            'name' => [new Length(['max'=>255]), new NotBlank()],
            'description' => [new Length(['max'=>65535])],
            'categoryID' => [new ExistingEntity(['propertyName' => 'id', 'entityClass'=>Category::class])],
            'videoFile' => [
                new File([
                    'mimeTypes'=>['mp4', 'video/3gpp'],
                    'maxSize' => '10M'
                ]),
                new NotBlank()
            ]
        ]);
    }

}