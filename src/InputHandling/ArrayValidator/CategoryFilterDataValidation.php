<?php


namespace App\InputHandling\ArrayValidator;


use App\InputHandling\ArrayValidator\ValidatorAbstract;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\Length;

class CategoryFilterDataValidation extends ValidatorAbstract
{

    public function constraints(): ?Constraint
    {
        $collection = new Collection([
            'name' => [new Length(['max'=>255])]
        ]);
        $collection->allowMissingFields = true;
        $collection->allowExtraFields = true;
        return $collection;
    }

}