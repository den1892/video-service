<?php


namespace App\InputHandling\ArrayValidator;


use App\Validator\ValidationResult;
use App\Validator\Validator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Validator\ValidatorBuilder;

abstract class ValidatorAbstract
{

    /**
     * @var ValidatorInterface
     */
    private $validator;


    public function __construct(Validator $validator)
    {
        $this->validator = $validator;
    }

    /**
     * @param $data
     * @param null $groups
     * @return ValidationResult
     */
    public function validateData($data, $groups = null): ValidationResult
    {
        return $this->validator->validate($data, $this->constraints(), $groups);
    }

    /**
     * @param Request $request
     * @param array $fileKeys
     * @param null $groups
     * @return ValidationResult
     */
    public function validateRequest(Request $request, array $fileKeys = [], $groups = null)
    {

        $data = $request->request->all();
        foreach ($fileKeys as $k){
            $data[$k] = $request->files->get($k);
        }

        return $this->validateData($data, $groups);
    }


    /**
     * @return Constraint|null
     */
     public function constraints(): ?Constraint
     {
         return null;
     }

}