<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190626212850 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE video DROP FOREIGN KEY FK_7CC7DA2CA7592BB9');
        $this->addSql('ALTER TABLE video DROP FOREIGN KEY FK_7CC7DA2CDB30DDED');
        $this->addSql('DROP INDEX IDX_7CC7DA2CDB30DDED ON video');
        $this->addSql('DROP INDEX IDX_7CC7DA2CA7592BB9 ON video');
        $this->addSql('ALTER TABLE video DROP categoryID, DROP ownerID');
        $this->addSql('ALTER TABLE video ADD CONSTRAINT FK_7CC7DA2C12469DE2 FOREIGN KEY (category_id) REFERENCES category (id)');
        $this->addSql('ALTER TABLE video ADD CONSTRAINT FK_7CC7DA2C7E3C61F9 FOREIGN KEY (owner_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_7CC7DA2C12469DE2 ON video (category_id)');
        $this->addSql('CREATE INDEX IDX_7CC7DA2C7E3C61F9 ON video (owner_id)');
        $this->addSql('ALTER TABLE category DROP FOREIGN KEY FK_64C19C11FDCEC3E');
        $this->addSql('DROP INDEX IDX_64C19C11FDCEC3E ON category');
        $this->addSql('ALTER TABLE category DROP creatorID');
        $this->addSql('ALTER TABLE category ADD CONSTRAINT FK_64C19C161220EA6 FOREIGN KEY (creator_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_64C19C161220EA6 ON category (creator_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE category DROP FOREIGN KEY FK_64C19C161220EA6');
        $this->addSql('DROP INDEX IDX_64C19C161220EA6 ON category');
        $this->addSql('ALTER TABLE category ADD creatorID INT DEFAULT NULL');
        $this->addSql('ALTER TABLE category ADD CONSTRAINT FK_64C19C11FDCEC3E FOREIGN KEY (creatorID) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_64C19C11FDCEC3E ON category (creatorID)');
        $this->addSql('ALTER TABLE video DROP FOREIGN KEY FK_7CC7DA2C12469DE2');
        $this->addSql('ALTER TABLE video DROP FOREIGN KEY FK_7CC7DA2C7E3C61F9');
        $this->addSql('DROP INDEX IDX_7CC7DA2C12469DE2 ON video');
        $this->addSql('DROP INDEX IDX_7CC7DA2C7E3C61F9 ON video');
        $this->addSql('ALTER TABLE video ADD categoryID INT DEFAULT NULL, ADD ownerID INT DEFAULT NULL');
        $this->addSql('ALTER TABLE video ADD CONSTRAINT FK_7CC7DA2CA7592BB9 FOREIGN KEY (categoryID) REFERENCES category (id)');
        $this->addSql('ALTER TABLE video ADD CONSTRAINT FK_7CC7DA2CDB30DDED FOREIGN KEY (ownerID) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_7CC7DA2CDB30DDED ON video (ownerID)');
        $this->addSql('CREATE INDEX IDX_7CC7DA2CA7592BB9 ON video (categoryID)');
    }
}
