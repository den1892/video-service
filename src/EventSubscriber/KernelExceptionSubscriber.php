<?php


namespace App\EventSubscriber;


use App\Api\ApiResponseService;
use App\Api\Models\ErrorsIterator;
use App\EventSubscriber\Exception\ContentLengthException;
use App\Serializer\SerializerBuilder;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;

class KernelExceptionSubscriber implements EventSubscriberInterface
{

    /**
     * @var ApiResponseService
     */
    private $apiResponseService;

    public function __construct(ApiResponseService $apiResponseService)
    {
        $this->apiResponseService = $apiResponseService;
    }

    /**
     * Returns an array of event names this subscriber wants to listen to.
     *
     * The array keys are event names and the value can be:
     *
     *  * The method name to call (priority defaults to 0)
     *  * An array composed of the method name to call and the priority
     *  * An array of arrays composed of the method names to call and respective
     *    priorities, or 0 if unset
     *
     * For instance:
     *
     *  * ['eventName' => 'methodName']
     *  * ['eventName' => ['methodName', $priority]]
     *  * ['eventName' => [['methodName1', $priority], ['methodName2']]]
     *
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return [
            'kernel.exception' => 'onKernelException'
        ];
    }

    public function onKernelException(ExceptionEvent $event)
    {

          $exception = $event->getException();
//        $errorIterator = new ErrorsIterator();
//        $errorIterator->addError($exception->getMessage(), 'root');
//
//        $preparedData = $this->apiResponseService->getPreparedData();
//        $preparedData
//            ->setMsg('Error')
//            ->setStatus(409)
//            ->setErrors($errorIterator);
//
//        $event->setResponse($this->apiResponseService->response($preparedData));
//
//        return;
         if ($exception instanceof ContentLengthException){

             $errorIterator = new ErrorsIterator();
             $errorIterator ->addError($exception->getMessage(), 'root');

             $preparedData = $this->apiResponseService->getPreparedData();
             $preparedData
                 ->setMsg('Error')
                 ->setStatus(409)
                 ->setErrors($errorIterator)
                 ;

             $event->setResponse($this->apiResponseService->response($preparedData));
         } else {

             $serializer = (new SerializerBuilder())->getSerializer();

             try {
                 $data = $serializer->normalize($exception, 'json', [
                     'attributes' => [
                         'message',
                         'trace' => ['file', 'line', 'function', 'class', 'type']
                     ]
                 ]);
             } catch (\Exception $e){
                 die($exception->getMessage());
             }

             $event->setResponse(new JsonResponse($data));

         }


    }
}