<?php


namespace App\EventSubscriber;


use App\Api\ApiResponseService;
use App\EventSubscriber\Exception\ContentLengthException;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class PhpWarningHandleSubscriber implements EventSubscriberInterface
{



    /**
     * Returns an array of event names this subscriber wants to listen to.
     *
     * The array keys are event names and the value can be:
     *
     *  * The method name to call (priority defaults to 0)
     *  * An array composed of the method name to call and the priority
     *  * An array of arrays composed of the method names to call and respective
     *    priorities, or 0 if unset
     *
     * For instance:
     *
     *  * ['eventName' => 'methodName']
     *  * ['eventName' => ['methodName', $priority]]
     *  * ['eventName' => [['methodName1', $priority], ['methodName2']]]
     *
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return [KernelEvents::CONTROLLER => 'onKernelController'];
    }

    public function onKernelController(ControllerEvent $event)
    {
        $contentLength = $event->getRequest()->server->get('CONTENT_LENGTH');
        $allowLength = ((int)ini_get('post_max_size') * 1024 * 1024);
        if (null !== $contentLength && $contentLength > $allowLength) {
            throw new ContentLengthException('Content length too large!');
        }
    }
}