<?php
/**
 * Created by PhpStorm.
 * User: basilliyc
 * Date: 05.06.19
 * Time: 13:50
 */

namespace App\EventSubscriber;


use App\Entity\Interfaces\EntityWithAuthUser;
use App\Entity\Interfaces\EntityWithDateInterface;
use App\User\AuthUser\AuthUserInterface;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;

class EntityEventSubscriber implements EventSubscriber
{


    /**
     * @var AuthUserInterface
     */
    private $authUser;

    public function __construct(AuthUserInterface $authUser)
    {
        $this->authUser = $authUser;
    }

    /**
     * Returns an array of events this subscriber wants to listen to.
     *
     * @return string[]
     */
    public function getSubscribedEvents()
    {
        return [
            Events::prePersist, Events::postLoad
        ];
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();


        if ($entity instanceof EntityWithDateInterface){
            $entity->setUpdatedAt(new \DateTime());
            if (null === $entity->getCreatedAt()){
                $entity->setCreatedAt(new \DateTime());
            }
        }
    }

    public function postLoad(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();

        if ($entity instanceof EntityWithAuthUser){
            $entity->setAuthUser($this->authUser);
        }
    }

}