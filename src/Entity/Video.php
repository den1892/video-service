<?php

namespace App\Entity;

use App\Entity\Interfaces\EntityWithAuthUser;
use App\Entity\Interfaces\EntityWithDateInterface;
use App\User\AuthUser\AuthUserInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\VideoRepository")
 */
class Video implements EntityWithDateInterface, EntityWithAuthUser
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="integer")
     */
    private $categoryID;

    /**
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="videos")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     */
    private $category;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $ownerID;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="videos"))
     * @ORM\JoinColumn(name="owner_id", referencedColumnName="id")
     */
    private $owner;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $file;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $avatar;

    /**
     * @var VideoLike[]
     * @ORM\OneToMany(targetEntity="VideoLike", mappedBy="video")
     */
    private $likes;

    /**
     * @var VideoLike[]
     */
    private $dislikesList;

    /**
     * @var VideoLike[]
     */
    private $likesList;
    /**
     * @var int
     */
    private $likesCount;

    /**
     * @var int
     */
    private $dislikesCount;

    /**
     * @var boolean
     */
    private $authUserLiked;

    /**
     * @var AuthUserInterface
     */
    private $authUser;
    /**
     * @var bool
     */
    private $authUserDisliked;

    public function __construct()
    {
        $this->likes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getCategoryID(): ?string
    {
        return $this->categoryID;
    }

    public function setCategoryID(string $categoryID): self
    {
        $this->categoryID = $categoryID;

        return $this;
    }

    public function getOwnerID(): ?int
    {
        return $this->ownerID;
    }

    public function setOwnerID(?int $ownerID): self
    {
        $this->ownerID = $ownerID;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(?\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(?\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    public function getFile(): ?string
    {
        return $this->file;
    }

    public function setFile(?string $file): self
    {
        $this->file = $file;

        return $this;
    }

    /**
     * @return Collection|VideoLike[]
     */
    public function getLikes(): Collection
    {
        return $this->likes;
    }

    public function addLike(VideoLike $like): self
    {
        if (!$this->likes->contains($like)) {
            $this->likes[] = $like;
            $like->setVideo($this);
        }

        return $this;
    }

    public function removeLike(VideoLike $like): self
    {
        if ($this->likes->contains($like)) {
            $this->likes->removeElement($like);
            // set the owning side to null (unless already changed)
            if ($like->getVideo() === $this) {
                $like->setVideo(null);
            }
        }

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getOwner(): ?User
    {
        return $this->owner;
    }

    public function setOwner(?User $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * @return VideoLike[]
     */
    public function getDislikesList(): array
    {
        if (null === $this->dislikesList) {
            $this->dislikesList = [];
            foreach ($this->likes as $like) {
                if (true === $like->getDisLike()) {
                    $this->dislikesList[] = $like;
                }
            }
        }
        return $this->dislikesList;
    }

    /**
     * @return VideoLike[]
     */
    public function getLikesList(): array
    {


        if (null === $this->likesList) {
            $this->likesList = [];
            foreach ($this->likes as $like) {
                if (true !== $like->getDisLike()) {
                    $this->likesList[] = $like;
                }
            }
        }
        return $this->likesList;
    }

    /**
     * @return int
     */
    public function getLikesCount(): int
    {
        if (null === $this->likesCount) {
            $this->likesCount = count($this->getLikesList());
        }
        return $this->likesCount;
    }

    /**
     * @return int
     */
    public function getDislikesCount(): int
    {
        if (null === $this->dislikesCount) {
            $this->dislikesCount = count($this->getDislikesList());
        }
        return $this->dislikesCount;
    }

    /**
     * @return bool
     */
    public function getAuthUserLiked(): bool
    {
        if (null === $this->authUserLiked) {
            $this->authUserLiked = false;

            if (null === $this->authUser || null === $this->authUser->currentUser()) {
                return $this->authUserLiked;
            }

            foreach ($this->getLikesList() as $like) {
                if ($like->getUserID() === $this->authUser->currentUser()->getID()) {
                    $this->authUserLiked = true;
                }
            }
        }
        return $this->authUserLiked;
    }

    /**
     * @return bool
     */
    public function getAuthUserDisliked(): bool
    {
        if (null === $this->authUserDisliked) {
            $this->authUserDisliked = false;

            if (null === $this->authUser || null === $this->authUser->currentUser()) {
                return $this->authUserDisliked;
            }

            foreach ($this->getDislikesList() as $like) {
                if ($like->getUserID() === $this->authUser->currentUser()->getID()) {
                    $this->authUserDisliked = true;
                }
            }
        }
        return $this->authUserDisliked;
    }

    /**
     * @param AuthUserInterface $authUser
     */
    public function setAuthUser(AuthUserInterface $authUser)
    {
        $this->authUser = $authUser;
    }

    public function getAvatar(): ?string
    {
        return $this->avatar;
    }

    public function setAvatar(?string $avatar): self
    {
        $this->avatar = $avatar;

        return $this;
    }
}
