<?php


namespace App\Entity\Interfaces;


use App\User\AuthUser\AuthUserInterface;

interface EntityWithAuthUser
{

    public function setAuthUser(AuthUserInterface $authUser);

}