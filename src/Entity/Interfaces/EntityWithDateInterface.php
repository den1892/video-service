<?php
/**
 * Created by PhpStorm.
 * User: basilliyc
 * Date: 05.06.19
 * Time: 13:39
 */

namespace App\Entity\Interfaces;


interface EntityWithDateInterface
{

    public function getCreatedAt(): ?\DateTimeInterface;

    public function setCreatedAt(?\DateTimeInterface $createdAt);

    public function getUpdatedAt(): ?\DateTimeInterface;

    public function setUpdatedAt(?\DateTimeInterface $createdAt);
}