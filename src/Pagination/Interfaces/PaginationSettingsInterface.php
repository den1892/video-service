<?php
/**
 * Created by PhpStorm.
 * User: basilliyc
 * Date: 04.06.19
 * Time: 14:00
 */

namespace App\Pagination\Interfaces;


interface PaginationSettingsInterface
{

    public function getCurrentPageNumber(): int;

    public function getItemsCount(): int;

    public function getLimitItems(): int;

    public function setItemsCount(int $itemsCount);

}