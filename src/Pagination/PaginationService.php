<?php
/**
 * Created by PhpStorm.
 * User: programmer_5
 * Date: 10.04.2019
 * Time: 17:05
 */

namespace App\Pagination;

use App\Pagination\Interfaces\PaginationSettingsInterface;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Tools\Pagination\Paginator;

class PaginationService
{

    /**
     * @param PaginationSettingsInterface $paginationSettings
     * @return Pagination
     */
    public function getPagination(PaginationSettingsInterface $paginationSettings): Pagination
    {
        return new Pagination($paginationSettings);
    }

    public function handleQuery(QueryBuilder $queryBuilder, PaginationSettingsInterface $paginationSettings)
    {
        $itemsCount = (new Paginator($queryBuilder))->count();
        $paginationSettings->setItemsCount($itemsCount);
        $queryBuilder
            ->setMaxResults($paginationSettings->getLimitItems())
            ->setFirstResult($paginationSettings->getLimitItems() * ($paginationSettings->getCurrentPageNumber() - 1));
    }

}