<?php
/**
 * Created by PhpStorm.
 * User: basilliyc
 * Date: 04.06.19
 * Time: 14:17
 */

namespace App\Pagination;


use App\Pagination\Interfaces\PaginationSettingsInterface;

class Pagination
{
    /**
     * @var PaginationSettingsInterface
     */
   private $paginationSettings;

   public function __construct(PaginationSettingsInterface $paginationSettings)
   {
       $this->paginationSettings = $paginationSettings;
   }

    public function pagesCount(): int
    {
        $count = $this->paginationSettings->getItemsCount();
        $limit = $this->paginationSettings->getLimitItems();
        return ceil($count/$limit);
    }

    public function validateCurrentPage(): bool
    {
        return $this->paginationSettings->getCurrentPageNumber() <= $this->pagesCount($this->paginationSettings);
    }

    public function getPaginationSettings(): PaginationSettingsInterface
    {
        return $this->paginationSettings;
    }
}