<?php
/**
 * Created by PhpStorm.
 * User: basilliyc
 * Date: 04.06.19
 * Time: 14:04
 */

namespace App\Pagination;


use App\Pagination\Interfaces\PaginationSettingsInterface;

class PaginationSettings implements PaginationSettingsInterface
{

    private $currentPageNumber = 1;

    private $itemsCount = 1;

    private $limitItems = 1;

    /**
     * @return int
     */
    public function getCurrentPageNumber(): int
    {
        return $this->currentPageNumber;
    }

    /**
     * @param int $currentPageNumber
     */
    public function setCurrentPageNumber(int $currentPageNumber): void
    {
        $this->currentPageNumber = $currentPageNumber > 0 ? $currentPageNumber : 1;
    }

    /**
     * @return int
     */
    public function getItemsCount(): int
    {
        return $this->itemsCount;
    }

    /**
     * @param int $itemsCount
     */
    public function setItemsCount(int $itemsCount): void
    {
        $this->itemsCount = $itemsCount > 0 ? $itemsCount : 1;
    }

    /**
     * @return int
     */
    public function getLimitItems(): int
    {
        return $this->limitItems;
    }

    /**
     * @param int $limitItems
     */
    public function setLimitItems(int $limitItems): void
    {
        $this->limitItems = $limitItems > 0 ? $limitItems : 1;
    }



}