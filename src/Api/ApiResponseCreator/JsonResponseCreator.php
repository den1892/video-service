<?php
/**
 * Created by PhpStorm.
 * User: basilliyc
 * Date: 05.06.19
 * Time: 23:39
 */

namespace App\Api\ApiResponseCreator;


use App\Api\Interfaces\ApiPreparedDataInterface;
use App\Api\Interfaces\ResponseCreatorInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class JsonResponseCreator implements ResponseCreatorInterface
{

    public function create(ApiPreparedDataInterface $data): Response
    {
        $serializer = new Serializer([new ObjectNormalizer()]);
        $responseData = [
            'msg' => $data->getMsg(),
            'errors' => $serializer->normalize($data->getErrors()),
            'data' => $data->getData()
        ];
        return  new JsonResponse($responseData, $data->getStatus());
    }
}