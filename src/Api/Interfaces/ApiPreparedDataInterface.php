<?php
/**
 * Created by PhpStorm.
 * User: basilliyc
 * Date: 05.06.19
 * Time: 23:22
 */

namespace App\Api\Interfaces;


interface ApiPreparedDataInterface
{

    public function getStatus(): int;

    public function getData();

    public function getMsg(): ?string;

    public function getErrors(): ErrorsIteratorInterface;

}