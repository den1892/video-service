<?php
/**
 * Created by PhpStorm.
 * User: basilliyc
 * Date: 05.06.19
 * Time: 23:51
 */

namespace App\Api\Interfaces;


interface ErrorsIteratorInterface extends \Iterator
{

    public function current(): ErrorInterface;

}