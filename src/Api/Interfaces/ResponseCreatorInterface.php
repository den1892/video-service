<?php
/**
 * Created by PhpStorm.
 * User: basilliyc
 * Date: 05.06.19
 * Time: 23:29
 */

namespace App\Api\Interfaces;

use Symfony\Component\HttpFoundation\Response;

interface ResponseCreatorInterface
{

    public function create(ApiPreparedDataInterface $data): Response;

}