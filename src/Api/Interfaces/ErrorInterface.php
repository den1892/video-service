<?php
/**
 * Created by PhpStorm.
 * User: basilliyc
 * Date: 06.06.19
 * Time: 0:07
 */

namespace App\Api\Interfaces;


interface ErrorInterface
{

    public function getMsg(): ?string;

    public function getPropertyPart(): ?string;

}