<?php
/**
 * Created by PhpStorm.
 * User: basilliyc
 * Date: 05.06.19
 * Time: 23:33
 */

namespace App\Api;


use App\Api\Interfaces\ApiPreparedDataInterface;
use App\Api\Interfaces\ResponseCreatorInterface;
use App\Api\Models\ApiPreparedData;
use App\Api\Models\Error;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\ConstraintViolationListInterface;

class ApiResponseService
{

    /**
     * @var ResponseCreatorInterface
     */
    private $responseCreator;

    public function __construct(ResponseCreatorInterface $responseCreator)
    {
        $this->responseCreator = $responseCreator;
    }

    /**
     * @return ApiPreparedData
     */
    public function getPreparedData(): ApiPreparedData
    {
       return new ApiPreparedData();
    }

    /**
     * @param ApiPreparedData $apiPreparedData
     * @param ConstraintViolationListInterface $constraintViolationList
     */
    public function loadErrorsFromConstraints(ApiPreparedData $apiPreparedData, ConstraintViolationListInterface $constraintViolationList)
    {
        foreach ($constraintViolationList as $item){

            $error = new Error();
            $error->setMsg($item->getMessage());
            $error->setPropertyPart($item->getPropertyPath());
            $apiPreparedData->addError($error);
        }
    }

    /**
     * @param ApiPreparedDataInterface $apiPreparedData
     * @return Response
     */
    public function response(ApiPreparedDataInterface $apiPreparedData): Response
    {
        return $this->responseCreator->create($apiPreparedData);
    }

}