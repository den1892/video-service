<?php
/**
 * Created by PhpStorm.
 * User: basilliyc
 * Date: 06.06.19
 * Time: 0:23
 */

namespace App\Api\Models;


use App\Api\Interfaces\ErrorInterface;

class Error implements ErrorInterface
{

    /**
     * @var string|null
     */
    private $msg;


    /**
     * @var string|null
     */
    private $propertyPart;

    /**
     * @return null|string
     */
    public function getMsg(): ?string
    {
        return $this->msg;
    }

    /**
     * @param null|string $msg
     */
    public function setMsg(?string $msg): void
    {
        $this->msg = $msg;
    }

    /**
     * @return null|string
     */
    public function getPropertyPart(): ?string
    {
        return $this->propertyPart;
    }

    /**
     * @param null|string $propertyPart
     */
    public function setPropertyPart(?string $propertyPart): void
    {
        $this->propertyPart = $propertyPart;
    }



}