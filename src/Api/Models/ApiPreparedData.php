<?php
/**
 * Created by PhpStorm.
 * User: basilliyc
 * Date: 04.06.19
 * Time: 13:17
 */

namespace App\Api\Models;


use App\Api\Interfaces\ApiPreparedDataInterface;
use App\Api\Interfaces\ErrorInterface;
use App\Api\Interfaces\ErrorsIteratorInterface;

class ApiPreparedData implements ApiPreparedDataInterface
{

    /**
     * @var int
     */
    private $status = 200;

    /**
     * @var string
     */
    private $msg='';

    /**
     * @var ErrorsIterator
     */
    private $errors = [];

    /**
     * @var null|array
     */
    private $data;


    public function __construct()
    {
        $this->errors = new ErrorsIterator();
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     * @return ApiPreparedData
     */
    public function setStatus(int $status): self
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return string
     */
    public function getMsg(): string
    {
        return $this->msg;
    }

    /**
     * @param string $msg
     * @return ApiPreparedData
     */
    public function setMsg(string $msg): self
    {
        $this->msg = $msg;
        return $this;
    }

    /**
     * @return ErrorsIteratorInterface
     */
    public function getErrors(): ErrorsIteratorInterface
    {
        return $this->errors;
    }

    /**
     * @param ErrorsIteratorInterface $errors
     * @return ApiPreparedData
     */
    public function setErrors(ErrorsIteratorInterface $errors): self
    {
        $this->errors = $errors;
        return $this;
    }

    /**
     * @param ErrorInterface $error
     * @return ApiPreparedData
     */
    public function addError(ErrorInterface $error): self
    {
       $this->errors->append($error);
       return $this;
    }

    /**
     * @return array|null
     */
    public function getData(): ?array
    {
        return $this->data;
    }

    /**
     * @param array|null $data
     * @return ApiPreparedData
     */
    public function setData(?array $data): self
    {
        $this->data = $data;
        return $this;
    }



}