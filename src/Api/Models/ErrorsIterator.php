<?php
/**
 * Created by PhpStorm.
 * User: basilliyc
 * Date: 06.06.19
 * Time: 0:11
 */

namespace App\Api\Models;

use App\Api\Interfaces\ErrorInterface;
use App\Api\Interfaces\ErrorsIteratorInterface;

class ErrorsIterator extends \ArrayIterator implements ErrorsIteratorInterface
{
    /**
     * @return ErrorInterface
     */
    public function current(): ErrorInterface
    {
        return parent::current();
    }

    /**
     * @param $msg
     * @param $property
     */
    public function addError($msg, $property)
    {
        $error = new Error();
        $error->setMsg($msg);
        $error->setPropertyPart($property);
        $this->append($error);
    }
}