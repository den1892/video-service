<?php


namespace App\Repository\Video;


use App\Entity\Video;
use App\Pagination\Interfaces\PaginationSettingsInterface;

interface VideoRepositoryInterface
{

    /**
     * @param Video $video
     */
    public function save(Video $video): void;

    /**
     * @param int $id
     * @return Video|null
     */
    public function findByID(int $id): ?Video;

    /**
     * @param PaginationSettingsInterface|null $paginationSettings
     * @param VideoFilterDataInterface|null $filterData
     * @return Video[]
     */
    public function getList(PaginationSettingsInterface $paginationSettings = null, VideoFilterDataInterface $filterData = null): array;

}