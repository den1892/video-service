<?php


namespace App\Repository\Video;


use App\Entity\Category;
use App\Entity\User;

interface VideoFilterDataInterface
{

    public function getCategory(): ?Category;

    public function getUser(): ?User;

    public function getUserLiked(): ?User;

    public function getUserDisliked(): ?User;

    public function getSearchString(): ?string;

}