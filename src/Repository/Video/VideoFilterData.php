<?php


namespace App\Repository\Video;


use App\Entity\Category;
use App\Entity\User;

class VideoFilterData implements VideoFilterDataInterface
{

    /**
     * @var Category|null;
     */
    private $category;

    /**
     * @var User|null
     */
    private $user;

    /**
     * @var User|null
     */
    private $userLiked;

    /**
     * @var User|null
     */
    private $userDisliked;
    /**
     * @var string|null
     */
    private $searchString;

    /**
     * @return Category|null
     */
    public function getCategory(): ?Category
    {
        return $this->category;
    }

    /**
     * @param Category|null $category
     */
    public function setCategory(?Category $category): void
    {
        $this->category = $category;
    }

    /**
     * @return User|null
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * @param User|null $user
     */
    public function setUser(?User $user): void
    {
        $this->user = $user;
    }

    /**
     * @return User|null
     */
    public function getUserLiked(): ?User
    {
        return $this->userLiked;
    }

    /**
     * @param User|null $userLiked
     */
    public function setUserLiked(?User $userLiked): void
    {
        $this->userLiked = $userLiked;
    }

    /**
     * @return string|null
     */
    public function getSearchString(): ?string
    {
        return $this->searchString;
    }

    /**
     * @param string|null $searchString
     */
    public function setSearchString(?string $searchString): void
    {
        $this->searchString = $searchString;
    }

    /**
     * @return User|null
     */
    public function getUserDisliked(): ?User
    {
        return $this->userDisliked;
    }

    /**
     * @param User|null $userDisliked
     */
    public function setUserDisliked(?User $userDisliked): void
    {
        $this->userDisliked = $userDisliked;
    }

}