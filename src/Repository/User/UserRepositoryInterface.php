<?php


namespace App\Repository\User;


use App\Entity\User;

interface UserRepositoryInterface
{
    /**
     * @param int|null $id
     * @return User|null
     */
    public function findByID(?int $id): ?User;

    /**
     * @param string|null $token
     * @return User|null
     */
    public function findByToken(?string $token): ?User;

    /**
     * @param User $user
     */
    public function saveUser(User $user): void;

    /**
     * @param string $email
     * @return User|null
     */
    public function findByEmail(string $email): ?User;
}