<?php

namespace App\Repository;

use App\Entity\Video;
use App\Pagination\Interfaces\PaginationSettingsInterface;
use App\Pagination\PaginationService;
use App\Repository\Video\VideoFilterDataInterface;
use App\Repository\Video\VideoRepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Video|null find($id, $lockMode = null, $lockVersion = null)
 * @method Video|null findOneBy(array $criteria, array $orderBy = null)
 * @method Video[]    findAll()
 * @method Video[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VideoRepository extends ServiceEntityRepository implements VideoRepositoryInterface
{
    /**
     * @var PaginationService
     */
    private $paginationService;

    public function __construct(RegistryInterface $registry, PaginationService $paginationService)
    {
        parent::__construct($registry, Video::class);
        $this->paginationService = $paginationService;
    }

    /**
     * @param PaginationSettingsInterface|null $paginationSettings
     * @param VideoFilterDataInterface|null $filterData
     * @return Video[]
     */
    public function getList(PaginationSettingsInterface $paginationSettings = null, VideoFilterDataInterface $filterData = null): array
    {
        $qb = $this
            ->createQueryBuilder('v')
            ->leftJoin('v.category', 'c')
            ->leftJoin('v.owner', 'o')
            ->leftJoin('v.likes', 'l')
            ->leftJoin('l.user', 'l_u')

//            ->addSelect('c')
//            ->addSelect('o')
//            ->addSelect('l')
//            ->addSelect('l_u')
        ;

        if (null !== $filterData->getCategory()){
            $qb->andWhere('v.categoryID = :categoryID')->setParameter('categoryID', $filterData->getCategory()->getId());
        }

        if (null !== $filterData->getUser()){
            $qb->andWhere('v.ownerID = :ownerID')->setParameter('ownerID', $filterData->getUser()->getID());
        }

        if (null !== $filterData->getUserLiked()){
            $qb
                ->andWhere('l.userID = :userLikedID')
                ->andWhere('l.disLike = :dislikeFalse')
                ->setParameter('userLikedID', $filterData->getUserLiked()->getID())
                ->setParameter('dislikeFalse', false)
            ;
        }

        if (null !== $filterData->getUserDisliked()){
            $qb
                ->andWhere('l.userID = :userDislikedID')
                ->andWhere('l.disLike = :dislikeTrue')
                ->setParameter('userDislikedID', $filterData->getUserDisliked()->getID())
                ->setParameter('dislikeTrue', true)
            ;
        }

        if (null !== $filterData->getSearchString()){
            $searchFields = ['v.name', 'v.description'];
            $exprLike = [];
            $expr = $qb->expr();
            foreach ($searchFields as $field){
                $exprLike[] = $expr->like($field, ':search');
                $qb->andWhere($expr->orX(...$exprLike))->setParameter('search', '%' . $filterData->getSearchString() . '%');
            }
        }

        $qb->groupBy('v.id');

        if (null !== $paginationSettings){
            $this->paginationService->handleQuery($qb, $paginationSettings);
        }

        return $qb->getQuery()->getResult();

    }

    /**
     * @param Video $video
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save(Video $video): void
    {
        $em = $this->getEntityManager();
        $em->persist($video);
        $em->flush();
    }

    /**
     * @param int $id
     * @return Video|null
     */
    public function findByID(int $id): ?Video
    {
        return $this->find($id);
    }
}
