<?php

namespace App\Repository;

use App\Entity\Category;
use App\Pagination\Interfaces\PaginationSettingsInterface;
use App\Pagination\PaginationService;
use App\Repository\Category\CategoryRepositoryInterface;
use App\Repository\Category\FilterDataInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Category|null find($id, $lockMode = null, $lockVersion = null)
 * @method Category|null findOneBy(array $criteria, array $orderBy = null)
 * @method Category[]    findAll()
 * @method Category[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategoryRepository extends ServiceEntityRepository implements CategoryRepositoryInterface
{
    /**
     * @var PaginationService
     */
    private $paginationService;

    public function __construct(RegistryInterface $registry, PaginationService $paginationService)
    {
        parent::__construct($registry, Category::class);
        $this->paginationService = $paginationService;
    }

    /**
     * @param Category $category
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save(Category $category): void
    {
        $em = $this->getEntityManager();
        $em->persist($category);
        $em->flush();
    }

    /**
     * @param int|null $categoryID
     * @return Category|null
     */
    public function findByID(?int $categoryID): ?Category
    {
        if (null === $categoryID){
            return null;
        }
        return $this->find($categoryID);
    }

    /**
     * @param PaginationSettingsInterface|null $paginationSettings
     * @param FilterDataInterface|null $filterData
     * @return Category[]
     */
    public function getList(?PaginationSettingsInterface $paginationSettings = null, ?FilterDataInterface $filterData = null): array
    {
        $qb = $this->createQueryBuilder('c');
        if (null !== $paginationSettings){
            $this->paginationService->handleQuery($qb, $paginationSettings);
        }

        if (null !== $filterData){
            if (null !== $filterData->getName()){
                $qb->andWhere('c.name like :name')->setParameter('name', '%'.$filterData->getName().'%');
            }
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * @param string|null $name
     * @return Category|null
     */
    public function findByName(?string $name): ?Category
    {
        return $this->findOneBy(['name' => $name]);
    }
}
