<?php


namespace App\Repository\Category;


use App\Entity\Category;
use App\Pagination\Interfaces\PaginationSettingsInterface;
use App\Pagination\PaginationSettings;

interface CategoryRepositoryInterface
{
    /**
     * @param Category $category
     */
    public function save(Category $category): void;

    /**
     * @param int|null $categoryID
     * @return Category|null
     */
    public function findByID(?int $categoryID): ?Category;

    /**
     * @param string|null $name
     * @return Category|null
     */
    public function findByName(?string $name): ?Category;

    /**
     * @param PaginationSettingsInterface|null $paginationSettings
     * @param FilterDataInterface|null $filterData
     * @return Category[]
     */
    public function getList(?PaginationSettingsInterface $paginationSettings=null, ?FilterDataInterface $filterData = null): array;

}