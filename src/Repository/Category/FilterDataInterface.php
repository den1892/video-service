<?php


namespace App\Repository\Category;


interface FilterDataInterface
{

    public function getName(): ?string;

}