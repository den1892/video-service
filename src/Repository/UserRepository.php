<?php

namespace App\Repository;

use App\Entity\User;
use App\Pagination\Interfaces\PaginationSettingsInterface;
use App\Pagination\PaginationService;
use App\Pagination\PaginationSettings;
use App\Repository\User\UserRepositoryInterface;
use App\Repository\Video\VideoFilterData;
use App\Repository\Video\VideoFilterDataInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository implements UserRepositoryInterface
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * @param int|null $id
     * @return User|null
     */
    public function findByID(?int $id): ?User
    {
        return $this->findOneBy(['id' => $id]);
    }

    /**
     * @param string|null $token
     * @return User|null
     */
    public function findByToken(?string $token): ?User
    {
        return $this->findOneBy(['token'=>$token]);
    }

    /**
     * @param string $email
     * @return User|null
     */
    public function findByEmail(string $email): ?User
    {
        return $this->findOneBy(['email'=>$email]);
    }

    /**
     * @param User $user
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function saveUser(User $user): void
    {
        $em = $this->getEntityManager();
        $em->persist($user);
        $em->flush();
    }


}
