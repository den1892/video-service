<?php

namespace App\DataFixtures;

use App\Entity\Area;
use App\Entity\Category;
use App\Entity\ContactPost;
use App\Entity\Culture;
use App\Entity\Elevator;
use App\Entity\FirmStatus;
use App\Entity\MailingStatus;
use App\Entity\Point;
use App\Entity\Region;
use App\Entity\User;
use App\Repository\CategoryRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\Id\UuidGenerator;
use Symfony\Component\Serializer\SerializerInterface;

class AppFixtures extends Fixture
{

    /**
     * @var SerializerInterface
     */
    private $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    public function load(ObjectManager $manager)
    {

        $firstUser = new User();
        $firstUser
            ->setEmail('admin@gmail.com')
            ->setFirstName('Admin User')
            ->setPassword('23e3defvrtv')
        ;

        $categories = ['Football', 'Tennis'];

        foreach ($categories as $name){
            $category = new Category();
            $category->setName($name);
            $manager->persist($category);
        }

        $manager->persist($firstUser);

        $manager->flush();
    }

}
