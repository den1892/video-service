<?php


namespace App\Serializer;


use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;

class NormalizerContextBuilder
{

    /**
     * @var callable
     */
    private $circularReferenceHandler;

    /**
     * @var callable
     */
    private $maxDepthHandler;

    /**
     * @param callable $circularReferenceHandler
     * @return NormalizerContextBuilder
     */
    public function setCircularReferenceHandler(callable $circularReferenceHandler): self
    {
        $this->circularReferenceHandler = $circularReferenceHandler;
        return $this;
    }

    /**
     * @param callable $maxDepthHandler
     * @return NormalizerContextBuilder
     */
    public function setMaxDepthHandler(callable $maxDepthHandler): self
    {
        $this->maxDepthHandler = $maxDepthHandler;
        return $this;
    }



    /**
     * @return array
     */
    public function getContext(): array
    {
        $prepareContext = [
           AbstractNormalizer::CIRCULAR_REFERENCE_HANDLER => $this->circularReferenceHandler,
           AbstractObjectNormalizer::MAX_DEPTH_HANDLER => $this->maxDepthHandler
        ];
        $context = [];
        foreach ($prepareContext as $key => $item) {
            if (null !== $item) {
                $context[$key] = $item;
            }
        }
        return $context;
    }

}