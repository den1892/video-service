<?php
/**
 * Created by PhpStorm.
 * User: basilliyc
 * Date: 04.06.19
 * Time: 16:38
 */

namespace App\Serializer;


use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactoryInterface;
use Symfony\Component\Serializer\NameConverter\NameConverterInterface;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class SerializerBuilder
{

    private $encoders = [];

    private $normalizers = [];

    private $nameConverter;

    /**
     * @var ClassMetadataFactoryInterface
     */
    private $classMetaDataFactory;

    /**
     * @var NormalizerContextBuilder
     */
    private $normalizerContextBuilder;

    public function __construct()
    {
        $this->normalizerContextBuilder = new NormalizerContextBuilder();
    }

    public function addEncoder($encoder): self
    {
        $this->encoders[get_class($encoder)] = $encoder;
        return $this;
    }

    public function addNormalizer($normalizer): self
    {
        $this->normalizers[get_class($normalizer)] = $normalizer;
        return $this;
    }

    /**
     * @param NameConverterInterface $nameConverter
     * @return SerializerBuilder
     */
    public function setNameConverter(NameConverterInterface $nameConverter): self
    {
        $this->nameConverter = $nameConverter;
        return $this;
    }

    /**
     * @param ClassMetadataFactoryInterface $classMetaDataFactory
     * @return SerializerBuilder
     */
    public function setClassMetaDataFactory(ClassMetadataFactoryInterface $classMetaDataFactory): SerializerBuilder
    {
        $this->classMetaDataFactory = $classMetaDataFactory;
        return $this;
    }


    /**
     * @return NormalizerContextBuilder
     */
    public function getNormalizerContextBuilder(): NormalizerContextBuilder
    {
        return $this->normalizerContextBuilder;
    }


    /**
     * @return Serializer
     */
    public function getSerializer(): Serializer
    {

        $normalizers = $this->normalizers;
        if (!isset($this->normalizers[ObjectNormalizer::class])){
            $objectNormalizer = new ObjectNormalizer($this->classMetaDataFactory, $this->nameConverter, null, null, null, null, $this->getNormalizerContextBuilder()->getContext());
            $normalizers[ObjectNormalizer::class] = $objectNormalizer;
        }

        return new Serializer(array_values($normalizers), array_values($this->encoders));
    }

}