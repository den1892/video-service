<?php
/**
 * Created by PhpStorm.
 * User: basilliyc
 * Date: 06.05.19
 * Time: 10:43
 */

namespace App\Serializer\NameConverters;


use Symfony\Component\Serializer\NameConverter\NameConverterInterface;

class NameConverterByAssociation implements NameConverterInterface
{

    /**
     * @var array
     */
    private $fields;

    public function __construct(array $fields=[])
    {
        $this->fields = $fields;
    }

    /**
     * @param array $fields
     */
    public function setFields(array $fields): void
    {
        $this->fields = $fields;
    }



    /**
     * Converts a property name to its normalized value.
     *
     * @param string $propertyName
     *
     * @return string
     */
    public function normalize($propertyName)
    {
        $result = array_search($propertyName,$this->fields);
        if (false === $result){
            $result = $propertyName;
        }
        return $result;
    }

    /**
     * Converts a property name to its denormalized value.
     *
     * @param string $propertyName
     *
     * @return string
     */
    public function denormalize($propertyName)
    {
        if (isset($this->fields[$propertyName])){
            return $this->fields[$propertyName];
        }
        return $propertyName;
    }

}