<?php


namespace App\Serializer;


use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;

class ContextBuilder
{

    private $attributes;

    private $ignoredAttributes;

    private $callbacks;

    private $existingData;

    private $enableMaxDepth;

    private $options = [];

    /**
     * @param array $attributes
     * @return ContextBuilder
     */
    public function setAttributes(array $attributes): self
    {
        $this->attributes = $attributes;
        return $this;
    }

    /**
     * @param array $ignoredAttributes
     * @return ContextBuilder
     */
    public function setIgnoredAttributes(array $ignoredAttributes): self
    {
        $this->ignoredAttributes = $ignoredAttributes;
        return $this;
    }


    /**
     * @param array $callbacks
     * @return ContextBuilder
     */
    public function setCallbacks(array $callbacks): self
    {
        $this->callbacks = $callbacks;
        return $this;
    }

    /**
     * @param mixed $existingData
     * @return ContextBuilder
     */
    public function setExistingData($existingData): self
    {
        $this->existingData = $existingData;
        return $this;
    }

    /**
     * @param bool $enableMaxDepth
     * @return ContextBuilder
     */
    public function setEnableMaxDepth(bool $enableMaxDepth): self
    {
        $this->enableMaxDepth = $enableMaxDepth;
        return $this;
    }

    /**
     * @param $key
     * @param $value
     */
    public function setOption(string $key, $value)
    {
        $this->options[$key] = $value;
    }



    /**
     * @return array
     */
    public function getContext(): array
    {
        $prepareContext = [
            'attributes' => $this->attributes,
            'ignored_attributes' => $this->ignoredAttributes,
            'callbacks' => $this->callbacks,
            'object_to_populate' => $this->existingData,
            'enable_max_depth' => $this->enableMaxDepth
        ];
        $context = [];
        foreach ($prepareContext as $key => $item) {
            if (null !== $item) {
                $context[$key] = $item;
            }
        }
        $context = array_merge($context, $this->options);

        return $context;
    }


}