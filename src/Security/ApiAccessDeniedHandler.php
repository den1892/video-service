<?php
/**
 * Created by PhpStorm.
 * User: basilliyc
 * Date: 11.05.19
 * Time: 17:20
 */

namespace App\Security;


use App\Api\ApiResponseService;
use App\Api\Models\ApiPreparedData;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Http\Authorization\AccessDeniedHandlerInterface;

class ApiAccessDeniedHandler implements AccessDeniedHandlerInterface
{

    /**
     * @var ApiResponseService
     */
    private $apiResponseService;

    public function __construct(ApiResponseService $apiResponseService)
    {

        $this->apiResponseService = $apiResponseService;
    }

    /**
     * Handles an access denied failure.
     *
     * @param Request $request
     * @param AccessDeniedException $accessDeniedException
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function handle(Request $request, AccessDeniedException $accessDeniedException)
    {
        $preparedData = new ApiPreparedData();
        $preparedData->setStatus(403);
        $preparedData->setMsg('Forbidden');
        return $this->apiResponseService->response($preparedData);
    }
}