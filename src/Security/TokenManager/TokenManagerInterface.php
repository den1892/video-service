<?php


namespace App\Security\TokenManager;


use Symfony\Component\HttpFoundation\Request;

interface TokenManagerInterface
{

    /**
     * @param Request $request
     * @return bool
     */
    public function validateRequest(Request $request): bool;

    /**
     * @param Request $request
     * @return string|null
     */
    public function getToken(Request $request): ?string;

}