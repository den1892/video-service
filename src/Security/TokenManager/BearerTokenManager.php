<?php


namespace App\Security\TokenManager;


use Symfony\Component\HttpFoundation\Request;

class BearerTokenManager implements TokenManagerInterface
{

    /**
     * @param Request $request
     * @return bool
     */
    public function validateRequest(Request $request): bool
    {
        return $request->headers->has('Authorization') || $request->query->has('token');
    }

    /**
     * @param Request $request
     * @return string|null
     */
    public function getToken(Request $request): ?string
    {
        $token = null;
        if ($request->headers->has('Authorization')){
            $token = $request->headers->get('Authorization');
            if (null !== $token && '' !== $token) {
                $token = substr($token, 7);
            }
        } else if ($request->query->has('token')) {
            $token_ = $request->get('token');
            $request->query->remove('token');
            if (is_string($token_)){
                $token = $token_;
            }
        }

        return $token;
    }
}